# Lableb Cloud Search's Javascript Client

Javascript library built to ease your integration with [Lableb](https://lableb.com).


# Overview

Lableb's client empower you with all the needed functions for a full search experience including: search, autocomplete, recommendation, indexing, deleting documents and feedbacks.

It's usage made easy for new users with a very good defaults, and the more you need from it, the more options you can use to suits your advanced usages.

> Note: This library is built with [Promises](https://github.com/getify/You-Dont-Know-JS/blob/1st-ed/async%20%26%20performance/ch3.md), and basically all functions return promises, so stay tuned.

To start, just create your very first client instance

```js
const lablebClient = await LablebClient();
```

> Note: For browser users you need to access `LablebClient` from `window.LablebSDK.LablebClient`.

Now `lablebClient` has all the functions needed for your application.

We'll do a simple search example:

```js
const { code, response, time } = await lablebClient.search({ 
  APIKey: API_KEY,
  platformName: PLATFORM_NAME,
  query: 'water',
});

const searchResults = response.results;
```

That's it, we just passed the required fields:
- Platform name
- API Key: used to authenticate your search request, visit [Lableb Dashboard](https://dashboard.lableb.com) to create a new one.
- query is what you're searching for, or basically your end-users' query.

Now you can do more than that, but sticking with the spirit of going fast and easy, that was all what you need.


# Options

When creating a new instance of the client you can pass nothing, or pass an option object which can save your time passing the same options over and over to the clients inner functions like search, autocomplete, recommend.


## Pass nothing

```js
const lablebClient = await LablebClient();
```

Now for every inner function you have to pass at least an `API Key` and a `Platform Name`

Search

```js
const { code, response, time } = await lablebClient.search({ 
  APIKey: API_KEY,
  platformName: PLATFORM_NAME,
  query: 'water',
});
```

Autocomplete

```js
const { code, response, time } = await lablebClient.autocomplete({ 
  APIKey: API_KEY,
  platformName: PLATFORM_NAME,
  query: 'water',
});
```

And so on...

## Pass the global options

You noticed that we repeated the shared options across many functions, to end that repetition, you can pass any shared option you find in this library to the main `LablebClient` that create the instance for you, and you're all set and ready.

```js
const lablebClient = await LablebClient({
  APIKey: API_KEY,
  platformName: PLATFORM_NAME,
});
```

Now for the other functions you just pass the required options


Search

```js
const { code, response, time } = await lablebClient.search({ 
  query: 'water',
});
```

Autocomplete

```js
const { code, response, time } = await lablebClient.autocomplete({ 
  query: 'water',
});
```

And so on...

## When passing options to an inner function might be useful?

Imagine you want to use the same options for all functions(search, autocomplete, recommend, ...etc) but for one function you have a single different option, you can pass the only different option value to this function which will override the previously saved value(passed to `LablebClient` globally) only for this function call.

```js
const lablebClient = await LablebClient({
  searchHandler: SEARCH_HANDLER_1,
});

const { code, response, time } = await lablebClient.search({ 
  searchHandler: SEARCH_HANDLER_2,
  query: 'water',
});
```

Now this search function call will have `searchHandler` value set to `SEARCH_HANDLER_2` as it override the global option passed to create the client.


## Options fields and types


Options are plain JS object, and all it's properties are optional

| property              | type   | default   | description |
| --------------------- | -------| --------- | --------------- |
| platformName          | string | -         | Your platform name as written at [Lableb Dashboard](https://dashboard.lableb.com) |
| indexName             | string | index     | The used data index name as created at [Lableb Dashboard \| Collections](https://dashboard.lableb.com) |
| APIKey                | string | -         | Search, Autocomplete, Recommend, and Feedback API Key |
| indexingAPIKey        | string | -         | Index, Delete API Key |
| jwtToken              | string | -         | you can use it instead of any API Key |
| searchHandler         | string | default   | The used search handler name as found at [Lableb Dashboard \| Collections \| Handlers](https://dashboard.lableb.com) |
| autocompleteHandler   | string | suggest   | The used autocomplete handler name as found at [Lableb Dashboard \| Collections \| Handlers](https://dashboard.lableb.com) |
| recommendHandler      | string | recommend | The used recommend handler name as found at [Lableb Dashboard \| Collections \| Handlers](https://dashboard.lableb.com) |



# Lableb's client 


## Search

Query your existing data at [Lableb](https://lableb.com) by sending text queries

```js
const lablebClient = await LablebClient({
    APIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});

const { code, response, time } = await lablebClient.search({
  query: 'water'
});

const searchResults = response.results;
```

Read about [Search API](./documentation/search.md) in details.






## Autocomplete

Get instant auto-complete results for the best typing experience for your end-users, where it can better help them find what they need.

```js
const lablebClient = await LablebClient({
    APIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});

const { code, response, time } = await lablebClient.autocomplete({
  query: 'wat'
});

const autocompleteResults = response.results;
```

Read about [Autocomplete API](./documentation/autocomplete.md) in details.




## Recommend

Smart applications display relevant data for their users, `Recommend` enable you to get the most relevant data(document/product/article...) your users are interacting with now, which will better increase your revenue, visits count, or the time a user spent on your application.

Pass down the data Id you want relevant data for, and the `recommend` function will return the best possible results.

```js
const lablebClient = await LablebClient({
    APIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});

const { code, response, time } = await lablebClient.recommend({
  id: '475'
});

const recommendResults = response.results;
```

Read about [Recommend API](./documentation/recommend.md) in details.



## Indexing

Adding or uploading your data to [Lableb](https://lableb.com) is called `indexing`.

Indexing is a very smooth experience, passing down the data as an array is all what it does, the only required thing is your data objects must match your data `schema` made at [Lableb Dashboard](https://dashboard.lableb.com).

Assuming your platform schema has an `id` and `title` as required fields.

```js
const lablebClient = await LablebClient({
    indexingAPIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});

await lablebClient.index({
  documents: [
    { id: '1', title: 'Finding water in Africa' },
    { id: '2', title: 'How important is water for daily life?' },
  ]
});
```

> Note: You need to use a special API Key for `indexing` and `delete`, we call it `indexingAPIKey`, you can create it inside [Lableb Dashboard](https://dashboard.lableb.com), where you choose to add `index` permission to the created API Key.

Read about [Indexing API](./documentation/indexing.md) in details.



## Delete

Remove your data at `Lableb` can be justified for many reasons, wrong data, stalled data, not needed anymore data, anyway you can absolutely remove any piece of data using its unique id.

```js
const lablebClient = await LablebClient({
    indexingAPIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});

await lablebClient.delete({
  documentId: '274'
});
```

Read about [Delete API](./documentation/delete.md) in details.



## Feedback

Sending feedback to Lableb help you study and understand your users behavior at a deeper level through various analytics at [Lableb Dashboard](https://dashboard.lableb.com).

You can access a feedback object that contains search, autocomplete, recommend feedbacks functions respectively where you have many options to send in the feedback, and you can either send a single feedback to Lableb at a time, or send a batch of feedbacks in one call. 

Each feedback object has two functions: `single` and `batch`

  - To Send single search feedback to Lableb use `feedback.search.single` function
  - To Send many search feedbacks at once to Lableb use `feedback.search.batch` function


Of course there is similar functions for autocomplete and recommend feedback respectively.



#### When feedback is usually used in application?

Feedback usually used on UI, when you do a search at Lableb you get back the search results and display them for your end users, when a user clicks or interact with the displayed result, you send a search feedback to Lableb, and so on, when your end users interacts with an autocomplete results, you send an autocomplete feedback, and when the end-users interact with a recommend result, you send a recommend feedback.


#### Feedback APIs:
- [Search Feedback](./documentation/search-feedback.md)
- [Autocomplete Feedback](./documentation/autocomplete-feedback.md)
- [Recommend Feedback](./documentation/recommend-feedback.md)


------------------------


## FAQ: 

  - What is a document at Lableb?
    
    It's a synonyms for data, product, article, and what you call it depends on your platform, whether it's e-commerce, blog, ...etc

  - How can I user promises in old browsers?

    if you're using an old version of EcmaScript and want to use the `async/await` syntax you easily do by install `babel-ployfill`

  - What is the difference between API Key and indexing API Key?

    API Key can only be used for `search`, `autocomplete`, `recommend`, and `feedbacks`, while indexing API Key is only used for `indexing` and `delete`.

  - Can I call `index` function in browser?

    Yes, but doing so will expose your Index API Key in developers tools, so we recommend leaving `indexing` and `delete` only for the server.