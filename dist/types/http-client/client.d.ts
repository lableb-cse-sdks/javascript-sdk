import { LablebHttpClientParams } from "./client.type";
export declare function LablebHttpClient(options: LablebHttpClientParams): Promise<any>;
