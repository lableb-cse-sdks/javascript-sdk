import * as yup from 'yup';
export declare const lablebHttpClientSchema: yup.ObjectSchema<import("yup/lib/object").Assign<import("yup/lib/object").ObjectShape, {
    method: import("yup/lib/string").RequiredStringSchema<string | undefined, Record<string, any>>;
    url: import("yup/lib/string").RequiredStringSchema<string | undefined, Record<string, any>>;
    body: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
    bodyType: yup.StringSchema<string, Record<string, any>, string>;
    params: import("yup/lib/object").OptionalObjectSchema<import("yup/lib/object").ObjectShape, Record<string, any>, import("yup/lib/object").TypeOfShape<import("yup/lib/object").ObjectShape>>;
    headers: import("yup/lib/object").OptionalObjectSchema<import("yup/lib/object").ObjectShape, Record<string, any>, import("yup/lib/object").TypeOfShape<import("yup/lib/object").ObjectShape>>;
}>, Record<string, any>, import("yup/lib/object").TypeOfShape<import("yup/lib/object").Assign<import("yup/lib/object").ObjectShape, {
    method: import("yup/lib/string").RequiredStringSchema<string | undefined, Record<string, any>>;
    url: import("yup/lib/string").RequiredStringSchema<string | undefined, Record<string, any>>;
    body: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
    bodyType: yup.StringSchema<string, Record<string, any>, string>;
    params: import("yup/lib/object").OptionalObjectSchema<import("yup/lib/object").ObjectShape, Record<string, any>, import("yup/lib/object").TypeOfShape<import("yup/lib/object").ObjectShape>>;
    headers: import("yup/lib/object").OptionalObjectSchema<import("yup/lib/object").ObjectShape, Record<string, any>, import("yup/lib/object").TypeOfShape<import("yup/lib/object").ObjectShape>>;
}>>, import("yup/lib/object").AssertsShape<import("yup/lib/object").Assign<import("yup/lib/object").ObjectShape, {
    method: import("yup/lib/string").RequiredStringSchema<string | undefined, Record<string, any>>;
    url: import("yup/lib/string").RequiredStringSchema<string | undefined, Record<string, any>>;
    body: import("yup/lib/mixed").MixedSchema<any, Record<string, any>, any>;
    bodyType: yup.StringSchema<string, Record<string, any>, string>;
    params: import("yup/lib/object").OptionalObjectSchema<import("yup/lib/object").ObjectShape, Record<string, any>, import("yup/lib/object").TypeOfShape<import("yup/lib/object").ObjectShape>>;
    headers: import("yup/lib/object").OptionalObjectSchema<import("yup/lib/object").ObjectShape, Record<string, any>, import("yup/lib/object").TypeOfShape<import("yup/lib/object").ObjectShape>>;
}>>>;
