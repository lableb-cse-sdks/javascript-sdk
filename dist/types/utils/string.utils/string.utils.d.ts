import { InputFacets } from "../../request/search/search.type";
export declare function camelCaseToSnackCaseObject(object: any): any;
/**
 * convert a facets object shape into a string or repeated facets string like
 * 'tags=greeting&tags=hello'
 */
export declare function convertFacetsObjectIntoString(facets: InputFacets): string;
