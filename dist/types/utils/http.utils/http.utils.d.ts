export declare function appendJwtTokenHeader(jwtToken?: string, existingHeaders?: any): any;
/**
 * convert js object into form data object
 *
 * @param {object} obj
 * @deprecated
 */
export declare function toFormData(obj: any): any;
