/**
 * take an object of key values, it keeps only truthy values depending on
 * the predicate function passed
 */
export declare function customPickBy(obj: any, func: (v: any) => boolean): any;
export declare function customIdentity(value: any): any;
