import { AutocompleteRequestParams } from "../../request/autocomplete/autocomplete.type";
import { GlobalRequestOptions } from "../../request/main/main.request.type";
import { RecommendRequestParams } from "../../request/recommend/recommend.type";
import { SearchRequestParams } from "../../request/search/search.type";
export declare function createAndTestSearchURL({ expectedURL, globalOptions, searchOptions, }: {
    searchOptions: SearchRequestParams;
    globalOptions: GlobalRequestOptions;
    expectedURL: string;
}): Promise<void>;
export declare function createAndTestAutocompleteURL({ expectedURL, globalOptions, autocompleteOptions, }: {
    autocompleteOptions: AutocompleteRequestParams;
    globalOptions: GlobalRequestOptions;
    expectedURL: string;
}): Promise<void>;
export declare function createAndTestRecommendURL({ expectedURL, globalOptions, recommendOptions, }: {
    recommendOptions: RecommendRequestParams;
    globalOptions: GlobalRequestOptions;
    expectedURL: string;
}): Promise<void>;
