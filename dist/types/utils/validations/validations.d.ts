export declare function isValidIPAddress(value?: string): boolean;
export declare function isValidDocumentIdForYup(value: any): boolean;
export declare function isValidHttpMethod(value?: string): boolean;
export declare function isValidBodyType(value?: string): boolean;
export declare function isTruthyString(value: string): boolean | "";
export declare function isValidIntegerForYup(value?: number): boolean;
