import { BaseRequestParams, BuildRequestResult } from "../../types";
import { FeedbackDocument } from "../feedback/feedback.document.type";
export interface BatchSearchFeedbackRequestParams extends Omit<BaseRequestParams, "sessionId" | "userId" | "userIp" | "userCountry"> {
    APIKey?: string;
    searchHandler?: string;
    documentsFeedbacks: FeedbackDocument[];
}
export interface SingleSearchFeedbackRequestParams extends Omit<BatchSearchFeedbackRequestParams, "documentsFeedbacks"> {
    documentFeedback: FeedbackDocument;
}
export interface BatchSearchFeedbackRequestResult extends BuildRequestResult {
    params: {
        apikey?: string;
    };
    body: FeedbackDocument[];
}
