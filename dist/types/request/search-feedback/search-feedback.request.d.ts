import { GlobalRequestOptions } from '../main/main.request.type';
import { BatchSearchFeedbackRequestParams, BatchSearchFeedbackRequestResult, SingleSearchFeedbackRequestParams } from './search-feedback.type';
export declare function buildSingleSearchFeedbackRequest(this: GlobalRequestOptions, params: SingleSearchFeedbackRequestParams): Promise<BatchSearchFeedbackRequestResult>;
export declare function buildBatchSearchFeedbackRequest(this: GlobalRequestOptions, params: BatchSearchFeedbackRequestParams): Promise<BatchSearchFeedbackRequestResult>;
