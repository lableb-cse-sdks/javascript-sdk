import * as yup from 'yup';
import { BatchSearchFeedbackRequestParams } from './search-feedback.type';
export declare const buildBatchSearchFeedbackRequestSchema: yup.SchemaOf<BatchSearchFeedbackRequestParams>;
