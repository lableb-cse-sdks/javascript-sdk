import { GlobalRequestOptions } from '../main/main.request.type';
import { BatchRecommendFeedbackRequestParams, BatchRecommendFeedbackRequestResult, SingleRecommendFeedbackRequestParams } from './recommend-feedback.type';
export declare function buildSingleRecommendFeedbackRequest(this: GlobalRequestOptions, params: SingleRecommendFeedbackRequestParams): Promise<BatchRecommendFeedbackRequestResult>;
export declare function buildBatchRecommendFeedbackRequest(this: GlobalRequestOptions, params: BatchRecommendFeedbackRequestParams): Promise<BatchRecommendFeedbackRequestResult>;
