import * as yup from 'yup';
import { BatchRecommendFeedbackRequestParams } from './recommend-feedback.type';
export declare const buildBatchRecommendFeedbackRequestSchema: yup.SchemaOf<BatchRecommendFeedbackRequestParams>;
