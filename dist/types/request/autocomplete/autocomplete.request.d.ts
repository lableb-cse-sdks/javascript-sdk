import { GlobalRequestOptions } from "../main/main.request.type";
import { AutocompleteRequestParams, AutocompleteRequestResult } from "./autocomplete.type";
export declare function buildAutocompleteRequest(this: GlobalRequestOptions, params: AutocompleteRequestParams): Promise<AutocompleteRequestResult>;
