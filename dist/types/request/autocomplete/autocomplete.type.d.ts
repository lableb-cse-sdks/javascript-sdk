import { BaseRequestParams, BuildRequestResult } from '../../types';
export interface AutocompleteRequestParams extends BaseRequestParams {
    APIKey?: string;
    autocompleteHandler?: string;
    query: string;
    limit?: number;
    filter?: string;
    sort?: string;
}
export interface AutocompleteRequestResult extends BuildRequestResult {
    params: Omit<AutocompleteRequestParams, "platformName" | "APIKey" | "indexName" | "autocompleteHandler" | "jwtToken" | "query"> & {
        q: string;
        apikey?: string;
    };
}
