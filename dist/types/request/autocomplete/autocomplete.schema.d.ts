import * as yup from 'yup';
import { AutocompleteRequestParams } from './autocomplete.type';
export declare const buildAutocompleteRequestSchema: yup.SchemaOf<AutocompleteRequestParams>;
