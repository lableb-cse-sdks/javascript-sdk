import { UnPromise } from "../../types";
import { LablebRequestBuilder } from "./main.request";
export interface GlobalRequestOptions {
    jwtToken?: string;
    APIKey?: string;
    indexingAPIKey?: string;
    platformName?: string;
    indexName?: string;
    searchHandler?: string;
    autocompleteHandler?: string;
    recommendHandler?: string;
}
export declare type LablebRequestBuilderType = UnPromise<ReturnType<typeof LablebRequestBuilder>>;
