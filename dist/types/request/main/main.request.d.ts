import { GlobalRequestOptions } from "./main.request.type";
export declare function LablebRequestBuilder(options?: GlobalRequestOptions): Promise<{
    index: (params: import("../..").IndexingRequestParams) => Promise<import("../..").IndexingRequestResult>;
    search: (params: import("../..").LablebSearchOptions) => Promise<import("../..").SearchRequestResult>;
    autocomplete: (params: import("../..").LablebAutocompleteOptions) => Promise<import("../..").AutocompleteRequestResult>;
    recommend: (params: import("../..").LablebRecommendOptions) => Promise<import("../..").RecommendRequestResult>;
    delete: (params: import("../..").DeleteRequestParams) => Promise<import("../..").DeleteRequestResult>;
    feedback: {
        search: {
            single: (params: import("../..").SingleSearchFeedbackRequestParams) => Promise<import("../..").BatchSearchFeedbackRequestResult>;
            batch: (params: import("../..").BatchSearchFeedbackRequestParams) => Promise<import("../..").BatchSearchFeedbackRequestResult>;
        };
        autocomplete: {
            single: (params: import("../..").SingleAutocompleteFeedbackRequestParams) => Promise<import("../..").BatchAutocompleteFeedbackRequestResult>;
            batch: (params: import("../..").BatchAutocompleteFeedbackRequestParams) => Promise<import("../..").BatchAutocompleteFeedbackRequestResult>;
        };
        recommend: {
            single: (params: import("../..").SingleRecommendFeedbackRequestParams) => Promise<import("../..").BatchRecommendFeedbackRequestResult>;
            batch: (params: import("../..").BatchRecommendFeedbackRequestParams) => Promise<import("../..").BatchRecommendFeedbackRequestResult>;
        };
    };
}>;
