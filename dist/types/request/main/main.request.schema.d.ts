import * as yup from 'yup';
import { BaseRequestParams, OptionalBaseRequestParams } from '../../types';
import { GlobalRequestOptions } from './main.request.type';
export declare const mainRequestSchema: yup.SchemaOf<GlobalRequestOptions>;
export declare const optionalBaseRequestSchema: yup.SchemaOf<OptionalBaseRequestParams>;
export declare const baseRequestSchema: yup.SchemaOf<BaseRequestParams>;
