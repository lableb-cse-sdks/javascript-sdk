import * as yup from 'yup';
import { DeleteRequestParams } from './delete.type';
export declare const buildIndexingRequestSchema: yup.SchemaOf<DeleteRequestParams>;
