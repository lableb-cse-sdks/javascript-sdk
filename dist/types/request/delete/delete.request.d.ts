import { GlobalRequestOptions } from '../main/main.request.type';
import { DeleteRequestParams, DeleteRequestResult } from './delete.type';
export declare function buildDeleteRequest(this: GlobalRequestOptions, params: DeleteRequestParams): Promise<DeleteRequestResult>;
