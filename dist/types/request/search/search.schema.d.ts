import * as yup from 'yup';
import { SearchRequestParams } from './search.type';
export declare const buildSearchRequestSchema: yup.SchemaOf<SearchRequestParams>;
