import { GlobalRequestOptions } from '../main/main.request.type';
import { SearchRequestParams, SearchRequestResult } from './search.type';
export declare function buildSearchRequest(this: GlobalRequestOptions, params: SearchRequestParams): Promise<SearchRequestResult>;
