import { BaseRequestParams, BuildRequestResult } from "../../types";
export interface InputFacets {
    [key: string]: string[];
}
export interface SearchRequestParams extends BaseRequestParams {
    APIKey?: string;
    searchHandler?: string;
    query: string;
    skip?: number;
    limit?: number;
    filter?: string;
    sort?: string;
    facets?: InputFacets;
}
export interface SearchRequestResult extends BuildRequestResult {
    params: Omit<SearchRequestParams, "platformName" | "APIKey" | "indexName" | "searchHandler" | "jwtToken" | "query"> & {
        q: string;
        apikey?: string;
    };
}
