import { BaseRequestParams, BuildRequestResult } from "../../types";
import { FeedbackDocument } from "../feedback/feedback.document.type";
export interface BatchAutocompleteFeedbackRequestParams extends Omit<BaseRequestParams, "sessionId" | "userId" | "userIp" | "userCountry"> {
    APIKey?: string;
    autocompleteHandler?: string;
    documentsFeedbacks: FeedbackDocument[];
}
export interface SingleAutocompleteFeedbackRequestParams extends Omit<BatchAutocompleteFeedbackRequestParams, "documentsFeedbacks"> {
    documentFeedback: FeedbackDocument;
}
export interface BatchAutocompleteFeedbackRequestResult extends BuildRequestResult {
    params: {
        apikey?: string;
    };
    body: FeedbackDocument[];
}
