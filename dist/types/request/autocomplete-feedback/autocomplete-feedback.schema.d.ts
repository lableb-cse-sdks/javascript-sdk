import * as yup from 'yup';
import { BatchAutocompleteFeedbackRequestParams } from './autocomplete-feedback.type';
export declare const buildBatchAutocompleteFeedbackRequestSchema: yup.SchemaOf<BatchAutocompleteFeedbackRequestParams>;
