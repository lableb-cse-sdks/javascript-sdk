import { GlobalRequestOptions } from '../main/main.request.type';
import { BatchAutocompleteFeedbackRequestParams, BatchAutocompleteFeedbackRequestResult, SingleAutocompleteFeedbackRequestParams } from './autocomplete-feedback.type';
export declare function buildSingleAutocompleteFeedbackRequest(this: GlobalRequestOptions, params: SingleAutocompleteFeedbackRequestParams): Promise<BatchAutocompleteFeedbackRequestResult>;
export declare function buildBatchAutocompleteFeedbackRequest(this: GlobalRequestOptions, params: BatchAutocompleteFeedbackRequestParams): Promise<BatchAutocompleteFeedbackRequestResult>;
