import { GlobalRequestOptions } from '../main/main.request.type';
import { IndexingRequestParams, IndexingRequestResult } from './indexing.type';
export declare function buildIndexingRequest(this: GlobalRequestOptions, params: IndexingRequestParams): Promise<IndexingRequestResult>;
