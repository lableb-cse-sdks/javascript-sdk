import { LablebDocument, LablebDocumentInput } from "../../core/lableb-client/lableb-client.type";
import { BuildRequestResult } from "../../types";
export interface IndexingRequestParams {
    jwtToken?: string;
    indexingAPIKey?: string;
    /**
     * Required if you didn't pass a platform name to the global lableb request builder
     *
     * if provided it will override the global platform name passed to Lableb request builder
     * otherwise we use the global platform name at Lableb request builder(if present, otherwise throw an error)
     */
    platformName?: string;
    indexName?: string;
    documents: LablebDocumentInput[];
}
export interface IndexingRequestResult extends BuildRequestResult {
    params: {
        apikey?: string;
    };
    body: LablebDocument[];
}
