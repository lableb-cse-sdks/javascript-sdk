import * as yup from 'yup';
import { LablebDocument } from '../../core/lableb-client/lableb-client.type';
import { IndexingRequestParams } from './indexing.type';
interface IndexingSchema extends Omit<IndexingRequestParams, "documents"> {
    /**
     * the optional key/value in LablebAPIDocument caused issue for yup
     * so we define it as an object which only contains "id" field
     */
    documents: (Pick<LablebDocument, "id">)[];
}
export declare const buildIndexingRequestSchema: yup.SchemaOf<IndexingSchema>;
export {};
