export interface FeedbackDocument {
    query: string;
    url?: string;
    itemId?: string | number;
    itemOrder?: number;
    sessionId?: string;
    userId?: string;
    userIp?: string;
    userCountry?: string;
}
export interface RecommendFeedbackDocument extends Omit<FeedbackDocument, "query" | "url" | "itemId"> {
    sourceId: number | string;
    sourceTitle?: string;
    sourceUrl?: string;
    targetId: number | string;
    targetTitle?: string;
    targetUrl?: string;
}
