import * as yup from 'yup';
import { FeedbackDocument, RecommendFeedbackDocument } from './feedback.document.type';
export declare const feedbackDocumentSchema: yup.SchemaOf<FeedbackDocument>;
export declare const recommendFeedbackDocumentSchema: yup.SchemaOf<RecommendFeedbackDocument>;
