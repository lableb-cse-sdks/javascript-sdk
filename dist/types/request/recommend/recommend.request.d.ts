import { GlobalRequestOptions } from '../main/main.request.type';
import { RecommendRequestParams, RecommendRequestResult } from './recommend.type';
export declare function buildRecommendRequest(this: GlobalRequestOptions, params: RecommendRequestParams): Promise<RecommendRequestResult>;
