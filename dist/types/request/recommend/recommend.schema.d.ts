import * as yup from 'yup';
import { RecommendRequestParams } from './recommend.type';
export declare const buildRecommendRequestSchema: yup.SchemaOf<RecommendRequestParams>;
