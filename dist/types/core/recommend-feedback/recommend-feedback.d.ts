import { BatchRecommendFeedbackRequestParams, SingleRecommendFeedbackRequestParams } from "../../request/recommend-feedback/recommend-feedback.type";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { RecommendFeedbackResponse } from "./recommend-feedback.type";
export declare function lablebClientSingleRecommendFeedback(this: LablebSDKContext, singleRecommendFeedbackOptions: SingleRecommendFeedbackRequestParams): Promise<LablebAPIResponseWrapper<RecommendFeedbackResponse>>;
export declare function lablebClientBatchRecommendFeedback(this: LablebSDKContext, batchRecommendFeedbackOptions: BatchRecommendFeedbackRequestParams): Promise<LablebAPIResponseWrapper<RecommendFeedbackResponse>>;
