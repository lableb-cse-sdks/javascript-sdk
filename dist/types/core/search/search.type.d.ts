import { LablebDocument, LablebDocumentWithFeedback } from "../lableb-client/lableb-client.type";
export interface SearchResponse {
    found_documents: number;
    results: LablebDocument[];
    facets: LablebFacets | null;
}
export interface SearchResponseWithFeedback {
    found_documents: number;
    results: LablebDocumentWithFeedback[];
    facets: LablebFacets | null;
}
export interface LablebFacet {
    value: string;
    count: number;
}
export interface LablebFacets {
    [key: string]: {
        buckets: LablebFacet[];
    };
}
