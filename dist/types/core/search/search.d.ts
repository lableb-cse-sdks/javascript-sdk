import { SearchRequestParams } from "../../request/search/search.type";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { SearchResponseWithFeedback } from "./search.type";
export declare function lablebClientSearch(this: LablebSDKContext, searchOptions: SearchRequestParams): Promise<LablebAPIResponseWrapper<SearchResponseWithFeedback>>;
