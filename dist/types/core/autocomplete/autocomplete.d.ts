import { AutocompleteRequestParams } from "../../request/autocomplete/autocomplete.type";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { AutocompleteResponse, AutocompleteResponseWithFeedback } from "./autocomplete.type";
export declare function lablebClientAutocomplete(this: LablebSDKContext, autocompleteOptions: AutocompleteRequestParams): Promise<LablebAPIResponseWrapper<AutocompleteResponseWithFeedback>>;
export declare function lablebClientNativeAutocomplete(this: LablebSDKContext, autocompleteOptions: AutocompleteRequestParams): Promise<LablebAPIResponseWrapper<AutocompleteResponse>>;
