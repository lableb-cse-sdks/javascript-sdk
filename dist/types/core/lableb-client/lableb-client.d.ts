import { GlobalRequestOptions } from "../../request/main/main.request.type";
export declare function LablebClient(globalOptions?: GlobalRequestOptions): Promise<{
    index: (indexingOptions: import("../..").IndexingRequestParams) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<null>>;
    delete: (deleteOptions: import("../..").DeleteRequestParams) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<null>>;
    search: (searchOptions: import("../..").LablebSearchOptions) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<import("../search/search.type").SearchResponseWithFeedback>>;
    autocomplete: (autocompleteOptions: import("../..").LablebAutocompleteOptions) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<import("../autocomplete/autocomplete.type").AutocompleteResponseWithFeedback>>;
    recommend: (recommendOptions: import("../..").LablebRecommendOptions) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<import("../recommend/recommend.type").RecommendResponseWithFeedback>>;
    feedback: {
        search: {
            single: (singleSearchFeedbackOptions: import("../..").SingleSearchFeedbackRequestParams) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<null>>;
            batch: (batchSearchFeedbackOptions: import("../..").BatchSearchFeedbackRequestParams) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<null>>;
        };
        autocomplete: {
            single: (singleAutocompleteFeedbackOptions: import("../..").SingleAutocompleteFeedbackRequestParams) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<null>>;
            batch: (batchAutocompleteFeedbackOptions: import("../..").BatchAutocompleteFeedbackRequestParams) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<null>>;
        };
        recommend: {
            single: (singleRecommendFeedbackOptions: import("../..").SingleRecommendFeedbackRequestParams) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<null>>;
            batch: (batchRecommendFeedbackOptions: import("../..").BatchRecommendFeedbackRequestParams) => Promise<import("./lableb-client.type").LablebAPIResponseWrapper<null>>;
        };
    };
    __defaults__: {
        API_BASE_URL: string | undefined;
        GLOBAL_DEFAULT_INDEX_NAME: string | undefined;
        GLOBAL_DEFAULT_SEARCH_HANDLER: string | undefined;
        GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER: string | undefined;
        GLOBAL_DEFAULT_RECOMMEND_HANDLER: string | undefined;
    };
}>;
