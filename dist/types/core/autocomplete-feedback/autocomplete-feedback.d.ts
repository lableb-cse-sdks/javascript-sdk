import { BatchAutocompleteFeedbackRequestParams, SingleAutocompleteFeedbackRequestParams } from "../../request/autocomplete-feedback/autocomplete-feedback.type";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { AutocompleteFeedbackResponse } from "./autocomplete-feedback.type";
export declare function lablebClientSingleAutocompleteFeedback(this: LablebSDKContext, singleAutocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams): Promise<LablebAPIResponseWrapper<AutocompleteFeedbackResponse>>;
export declare function lablebClientBatchAutocompleteFeedback(this: LablebSDKContext, batchAutocompleteFeedbackOptions: BatchAutocompleteFeedbackRequestParams): Promise<LablebAPIResponseWrapper<AutocompleteFeedbackResponse>>;
