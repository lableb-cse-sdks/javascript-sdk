import { DeleteRequestParams } from "../../request/delete/delete.type";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { DeleteResponse } from "./delete.type";
export declare function lablebClientDelete(this: LablebSDKContext, deleteOptions: DeleteRequestParams): Promise<LablebAPIResponseWrapper<DeleteResponse>>;
