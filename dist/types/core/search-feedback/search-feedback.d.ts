import { BatchSearchFeedbackRequestParams, SingleSearchFeedbackRequestParams } from "../../request/search-feedback/search-feedback.type";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { SearchFeedbackResponse } from "./search-feedback.type";
export declare function lablebClientSingleSearchFeedback(this: LablebSDKContext, singleSearchFeedbackOptions: SingleSearchFeedbackRequestParams): Promise<LablebAPIResponseWrapper<SearchFeedbackResponse>>;
export declare function lablebClientBatchSearchFeedback(this: LablebSDKContext, batchSearchFeedbackOptions: BatchSearchFeedbackRequestParams): Promise<LablebAPIResponseWrapper<SearchFeedbackResponse>>;
