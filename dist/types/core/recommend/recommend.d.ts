import { RecommendRequestParams } from "../../request/recommend/recommend.type";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { RecommendResponseWithFeedback } from "./recommend.type";
export declare function lablebClientRecommend(this: LablebSDKContext, recommendOptions: RecommendRequestParams): Promise<LablebAPIResponseWrapper<RecommendResponseWithFeedback>>;
