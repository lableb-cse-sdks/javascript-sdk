import { LablebDocumentWithRecommendFeedback } from "../lableb-client/lableb-client.type";
import { LablebFacets, SearchResponse } from "../search/search.type";
export interface RecommendResponse extends SearchResponse {
}
export interface RecommendResponseWithFeedback {
    found_documents: number;
    results: LablebDocumentWithRecommendFeedback[];
    facets: LablebFacets | null;
}
