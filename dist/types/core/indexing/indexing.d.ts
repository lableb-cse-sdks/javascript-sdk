import { IndexingRequestParams } from "../../request/indexing/indexing.type";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { IndexingResponse } from "./indexing.type";
export declare function lablebClientIndexing(this: LablebSDKContext, indexingOptions: IndexingRequestParams): Promise<LablebAPIResponseWrapper<IndexingResponse>>;
