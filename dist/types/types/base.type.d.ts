import { ALLOWED_HTTP_METHOD } from "../http-client/client.type";
export interface OptionalBaseRequestParams {
    sessionId?: string;
    userId?: string;
    userIp?: string;
    userCountry?: string;
}
export interface BaseRequestParams extends OptionalBaseRequestParams {
    platformName?: string;
    indexName?: string;
    jwtToken?: string;
    APIKey?: string;
}
export interface BuildRequestResult {
    url: string;
    params: any;
    headers: any;
    method: ALLOWED_HTTP_METHOD;
}
