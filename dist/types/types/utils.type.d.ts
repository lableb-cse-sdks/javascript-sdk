/**
 * Partial by:
 * https://stackoverflow.com/questions/43159887/make-a-single-property-optional-in-typescript
 */
export declare type OptionalProperties<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
export declare type UnPromise<T extends Promise<any>> = T extends Promise<infer U> ? U : never;
