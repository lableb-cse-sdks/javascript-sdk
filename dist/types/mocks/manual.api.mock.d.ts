import { AutocompleteFeedbackResponse } from '../core/autocomplete-feedback/autocomplete-feedback.type';
import { AutocompleteResponse } from '../core/autocomplete/autocomplete.type';
import { DeleteResponse } from '../core/delete/delete.type';
import { IndexingResponse } from '../core/indexing/indexing.type';
import { RecommendFeedbackResponse } from '../core/recommend-feedback/recommend-feedback.type';
import { RecommendResponseWithFeedback } from '../core/recommend/recommend.type';
import { SearchFeedbackResponse } from '../core/search-feedback/search-feedback.type';
import { SearchResponse } from '../core/search/search.type';
import { AutocompleteRequestResult } from '../request/autocomplete/autocomplete.type';
import { RecommendRequestResult } from '../request/recommend/recommend.type';
import { SearchRequestResult } from '../request/search/search.type';
export declare const MOCK_BACKEND_API: {
    SEARCH_URL: string;
    search: ({ params, }: {
        params: SearchRequestResult['params'];
    }) => SearchResponse;
    AUTOCOMPLETE_URL: string;
    autocomplete: ({ params, }: {
        params: AutocompleteRequestResult['params'];
    }) => AutocompleteResponse;
    RECOMMEND_URL: string;
    recommend: ({ params, }: {
        params: RecommendRequestResult['params'];
    }) => RecommendResponseWithFeedback;
    INDEX_URL: string;
    index: ({ params, }: {
        params: any;
    }) => IndexingResponse;
    DELETE_URL: string;
    delete: ({ params, }: {
        params: any;
    }) => DeleteResponse;
    SEARCH_FEEDBACK_URL: string;
    searchFeedback: ({ params, }: {
        params: any;
    }) => SearchFeedbackResponse;
    AUTOCOMPLETE_FEEDBACK_URL: string;
    autocompleteFeedback: ({ params, }: {
        params: any;
    }) => AutocompleteFeedbackResponse;
    RECOMMEND_FEEDBACK_URL: string;
    recommendFeedback: ({ params, }: {
        params: any;
    }) => RecommendFeedbackResponse;
};
