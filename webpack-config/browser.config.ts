import * as webpack from 'webpack';
import * as path from 'path';

// require('dotenv').config({
//     path: process.env.MODE == 'development' ?
//         path.join(__dirname, '../.env.development')
//         :
//         path.join(__dirname, '../.env.production')
// });

export const BrowserConfiguration: webpack.Configuration = {
    target: 'web',
    output: {
        path: path.join(__dirname, '../dist/'),
        filename: 'LablebSDK.min.js',
        library: 'LablebSDK',
        libraryTarget: 'umd',
        umdNamedDefine: true,
        globalObject: 'this',
    },
    plugins: [
        new webpack.DefinePlugin(

                {
                "process.env.LABLEB_JS_SDK": "'LABLEB-JS-SDK-DEV-MODE'",
                "process.env.API_BASE_URL": "'https://api.lableb.com/v2'",
                "process.env.GLOBAL_DEFAULT_INDEX_NAME": "'index'",
                "process.env.GLOBAL_DEFAULT_SEARCH_HANDLER": "'default'",
                "process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER": "'suggest'",
                "process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER": "'recommend'",
            }

            // Object.keys(process.env)
            //     .reduce((prevObject, nextKey) => ({
            //         ...prevObject,
            //         [`process.env.${nextKey}`]: `${process.env[nextKey]}`
            //     }), {})

        ),
    ],
}
