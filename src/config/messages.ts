export const MESSAGES = {
    PLATFORM_NAME_IS_REQUIRED: 'Platform name is a required field',


    INDEX_NAME_IS_REQUIRED: 'Index name is a required field',


    API_KEY_IS_REQUIRED: 'API Key is a required field',


    INDEXING_API_KEY_IS_REQUIRED: 'Indexing API Key is a required field',


    JWT_TOKEN_IS_REQUIRED: 'JWT Token is a required field',


    INVALID_IP_ADDRESS: 'Please provide a valid IP Address',

    QUERY_IS_REQUIRED: 'query is a required field',

    DOCUMENTS_IS_REQUIRED: 'documents is a required field',
    DOCUMENTS_LENGTH_IS_INVALID: 'documents field must have at least 1 items',
    DOCUMENT_ID_IS_REQUIRED: 'document id is a required field',

    IS_INVALID_DOCUMENT_ID: 'document id passed must be none empty string',

    COUNTRY_NAME_LENGTH_IS_INVALID: 'user country should be a string of two letters(iso 3166)',

    URL_IS_NOT_VALID: 'URL is not valid',

    SKIP_IS_NOT_INTEGER: 'Please provide an integer value for "skip"',
    LIMIT_IS_NOT_INTEGER: 'Please provide an integer value for "limit"',
    USER_ID_IS_NOT_INTEGER: 'Please provide an integer value for "user id"',
    INVALID_USER_ID: 'User id must be none empty string',
    ITEM_ORDER_IS_NOT_INTEGER: 'Please provide an integer value for "item order"',

    FEEDBACK_DOCUMENTS_LENGTH_INVALID: 'Feedback documents array should contains at least one item',
    FEEDBACK_DOCUMENTS_ARE_REQUIRED: 'Feedback documents array is required',

    SOURCE_ID_IS_REQUIRED: 'Source document id(which used to fetch the recommend results for) is required for the recommend feedback to complete',
    SOURCE_ID_IS_INVALID_DOCUMENT_ID: 'Source document id passed must be none empty string',

    TARGET_ID_IS_REQUIRED: 'Target document id(which the user clicks from within the recommend results displayed) is required for the recommend feedback to complete',
    TARGET_ID_IS_INVALID_DOCUMENT_ID: 'Source document id passed must be none empty string',
    TARGET_ID_MUST_BE_DIFFERENT_THAN_SOURCE_ID: 'Target Id can\'t be equal to source id in recommend feedback request',

    TARGET_TITLE_LENGTH_IS_INVALID: 'Target title document can\'t be empty string',
    SOURCE_TITLE_LENGTH_IS_INVALID: 'Source title document can\'t be empty string',


    WRONG_HTTP_METHOD: 'Wrong http method',
    BODY_TYPE_SHOULD_BE_JSON_OR_FORM_DATA: 'Body type should be "json" or "form-data"',
    BODY_IS_NOT_ALLOWED_FOR_GET_OR_DELETE: 'Body is not allowed for get & delete requests',
    BODY_IS_REQUIRED: 'Body is required',

}