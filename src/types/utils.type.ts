/**
 * Partial by:
 * https://stackoverflow.com/questions/43159887/make-a-single-property-optional-in-typescript
 */
export type OptionalProperties<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>



export type UnPromise<T extends Promise<any>> = T extends Promise<infer U> ? U : never;
