import { LablebClient } from '../..';
import { MESSAGES } from '../../config/messages';



describe('Test Auth', () => {

    test('autocomplete at lableb cloud without jwt', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                jwtToken: 'bad jwt'
            });

            await lablebClient.autocomplete({
                query: '*',
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });


    test('autocomplete at lableb cloud without api key', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                APIKey: 'bad api key'
            });

            await lablebClient.autocomplete({
                query: '*',
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });
});



describe('Test Autocomplete Request', () => {


    test('Autocomplete at lableb cloud', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.autocomplete({
            query: 'hello'
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');
    });


    test('Autocomplete at lableb cloud with all optional options', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.autocomplete({
            query: 'hello',
            filter: 'title:nice',
            limit: 2,
            sessionId: '92492mfa',
            sort: 'title asc',
            userId: '42',
            userIp: '172.65.23.22',
            userCountry: 'AE',
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');
    });




    test('Autocomplete at lableb cloud with feedback', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.autocomplete({
            query: 'hello'
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');


        if (response.results.length) {
            expect(response.results[0]).toHaveProperty('feedback');
            expect(response.results[0].feedback.itemId).toEqual(response.results[0].id);

            // @todo
            // const feedbackResponse = await lablebClient.feedback.autocomplete.single({
            //     documentFeedback: response.results[0].feedback,
            // });

            // expect(feedbackResponse.code).toBe(200);
            // expect(feedbackResponse.time).toBeGreaterThan(0);
            // expect(feedbackResponse.response).toBeNull();
        }

    });
});


describe('Bad Inputs', () => {


    test('Autocomplete at lableb cloud with no query', async () => {
        try {
            const lablebClient = await LablebClient({
                APIKey: process.env.API_KEY,
                platformName: process.env.PLATFORM_NAME,
            });

            await lablebClient.search({
                query: '',
            });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.QUERY_IS_REQUIRED);
        }
    });

});

