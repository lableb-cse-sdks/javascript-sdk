import { SearchResponse, SearchResponseWithFeedback } from "../search/search.type";

export interface AutocompleteResponse extends SearchResponse { }
export interface AutocompleteResponseWithFeedback extends SearchResponseWithFeedback { }