import { LablebHttpClient } from "../../http-client";
import { AutocompleteRequestParams } from "../../request/autocomplete/autocomplete.type";
import { camelCaseToSnackCaseObject, customIdentity, customPickBy } from "../../utils";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { AutocompleteResponse, AutocompleteResponseWithFeedback } from "./autocomplete.type";


export async function lablebClientAutocomplete(

    this: LablebSDKContext,

    autocompleteOptions: AutocompleteRequestParams

): Promise<LablebAPIResponseWrapper<AutocompleteResponseWithFeedback>> {


    return injectFeedbackDataIntoAutocompleteResponse(
        autocompleteOptions,
        await lablebClientNativeAutocomplete.bind(this)(autocompleteOptions)
    )
}


export async function lablebClientNativeAutocomplete(

    this: LablebSDKContext,

    autocompleteOptions: AutocompleteRequestParams

): Promise<LablebAPIResponseWrapper<AutocompleteResponse>> {

    const { url, headers, method, params } = await this.requestBuilder.autocomplete(autocompleteOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
    });
}



function injectFeedbackDataIntoAutocompleteResponse(

    autocompleteOptions: AutocompleteRequestParams,

    searchResponse: LablebAPIResponseWrapper<AutocompleteResponse>

): LablebAPIResponseWrapper<AutocompleteResponseWithFeedback> {

    return {
        ...searchResponse,
        response: {
            ...searchResponse.response,
            results: searchResponse.response.results.map((document, index) => ({
                ...document,
                feedback: customPickBy({
                    query: autocompleteOptions.query,
                    itemId: document.id,
                    itemOrder: index,
                    sessionId: autocompleteOptions.sessionId,
                    url: document.url,
                    userCountry: autocompleteOptions.userCountry,
                    userId: autocompleteOptions.userId,
                    userIp: autocompleteOptions.userIp,
                }, customIdentity),
            }))
        }
    }
}
