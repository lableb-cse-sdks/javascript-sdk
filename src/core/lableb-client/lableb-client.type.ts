import { FeedbackDocument, RecommendFeedbackDocument } from "../../request/feedback/feedback.document.type";
import { LablebRequestBuilderType } from "../../request/main/main.request.type";

export interface LablebDocument {
    id: string,
    [key: string]: any,
}

export interface LablebDocumentInput {
    id: string | number,
    [key: string]: any,
}

export interface LablebDocumentWithFeedback {
    id: string,
    feedback: FeedbackDocument,
    [key: string]: any,
}

export interface LablebDocumentWithRecommendFeedback {
    id: string,
    feedback: RecommendFeedbackDocument,
    [key: string]: any,
}


export interface LablebSDKContext {
    requestBuilder: LablebRequestBuilderType
}

export interface LablebAPIResponseWrapper<T> {
    time: number,
    code: number,
    response: T
}