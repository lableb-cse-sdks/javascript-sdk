import { MESSAGES } from "../../config/messages";
import { LablebClient } from "./lableb-client";


describe('Test Auth', () => {

    test('recommend at lableb cloud without jwt', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                jwtToken: 'bad jwt'
            });

            await lablebClient.recommend({
                id: '1',
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });


    test('recommend at lableb cloud without api key', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                APIKey: 'bad api key'
            });

            await lablebClient.recommend({
                id: '1'
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });
});


describe('Test Recommend Request', () => {

    test('Recommend at lableb cloud', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.recommend({
            id: 1,
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');
    });


    test('Recommend at lableb cloud with all optional options', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.recommend({
            id: 1,
            filter: 'title:nice',
            limit: 2,
            sessionId: '92492mfa',
            sort: 'title asc',
            userId: '42',
            userIp: '172.65.23.22',
            userCountry: 'AE',
            title: 'title',
            url: 'https://example.com'
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');
    });






    test('Recommend at lableb cloud with feedback', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.recommend({
            id: 3,
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');


        if (response.results.length) {
            expect(response.results[0]).toHaveProperty('feedback');
            expect(response.results[0].feedback.sourceId).toEqual(3);
            expect(response.results[0].feedback.targetId).toEqual(response.results[0].id);

            const feedbackResponse = await lablebClient.feedback.recommend.single({
                documentFeedback: response.results[0].feedback,
            });

            expect(feedbackResponse.code).toBe(200);
            expect(feedbackResponse.time).toBeGreaterThan(0);
            expect(feedbackResponse.response).toBeNull();
        }
    });

});



describe('Bad Inputs', () => {

    test('Recommend at lableb cloud with no query', async () => {
        try {
            const lablebClient = await LablebClient({
                APIKey: process.env.API_KEY,
                platformName: process.env.PLATFORM_NAME,
            });

            await lablebClient.recommend({
                id: '',
            });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.IS_INVALID_DOCUMENT_ID);
        }
    });

});