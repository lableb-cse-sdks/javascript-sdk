import { LablebRequestBuilder } from "../../request/main/main.request";
import { GlobalRequestOptions } from "../../request/main/main.request.type";
import { lablebClientBatchAutocompleteFeedback, lablebClientSingleAutocompleteFeedback } from "../autocomplete-feedback/autocomplete-feedback";
import { lablebClientAutocomplete } from "../autocomplete/autocomplete";
import { lablebClientDelete } from "../delete/delete";
import { lablebClientIndexing } from "../indexing/indexing";
import { lablebClientBatchRecommendFeedback, lablebClientSingleRecommendFeedback } from "../recommend-feedback/recommend-feedback";
import { lablebClientRecommend } from "../recommend/recommend";
import { lablebClientBatchSearchFeedback, lablebClientSingleSearchFeedback } from "../search-feedback/search-feedback";
import { lablebClientSearch } from "../search/search";


export async function LablebClient(globalOptions: GlobalRequestOptions = {}) {

    const requestBuilder = await LablebRequestBuilder(globalOptions);
    const clientContext = { requestBuilder };

    return {
        index: lablebClientIndexing.bind(clientContext),
        delete: lablebClientDelete.bind(clientContext),
        search: lablebClientSearch.bind(clientContext),
        autocomplete: lablebClientAutocomplete.bind(clientContext),
        recommend: lablebClientRecommend.bind(clientContext),
        feedback: {
            search: {
                single: lablebClientSingleSearchFeedback.bind(clientContext),
                batch: lablebClientBatchSearchFeedback.bind(clientContext),
            },
            autocomplete: {
                single: lablebClientSingleAutocompleteFeedback.bind(clientContext),
                batch: lablebClientBatchAutocompleteFeedback.bind(clientContext),
            },
            recommend: {
                single: lablebClientSingleRecommendFeedback.bind(clientContext),
                batch: lablebClientBatchRecommendFeedback.bind(clientContext),
            },
        },
        __defaults__: {
            "API_BASE_URL": process.env.API_BASE_URL,
            "GLOBAL_DEFAULT_INDEX_NAME": process.env.GLOBAL_DEFAULT_INDEX_NAME,
            "GLOBAL_DEFAULT_SEARCH_HANDLER": process.env.GLOBAL_DEFAULT_SEARCH_HANDLER,
            "GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER": process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER,
            "GLOBAL_DEFAULT_RECOMMEND_HANDLER": process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER,
        }
    }
}
