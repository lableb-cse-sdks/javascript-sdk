import { LablebClient } from "../lableb-client/lableb-client";



describe('Test Auth', () => {

    test('Recommend feedback at lableb cloud without jwt', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                jwtToken: 'bad jwt'
            });

            await lablebClient.feedback.recommend.single({
                documentFeedback: {
                    sourceId: 1,
                    targetId: 2,
                }
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });


    test('Recommend feedback at lableb cloud without api key', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                APIKey: 'bad api key',
            });

            await lablebClient.feedback.recommend.single({
                documentFeedback: {
                    sourceId: 141,
                    sourceTitle: 'Searching for water',
                    sourceUrl: 'https://example.com/articles/searching-for-water',

                    targetId: 984,
                    targetTitle: 'How to preserve water consumption by 20%',
                    targetUrl: 'https://example.com/articles/preserve-water-consumption',

                    itemOrder: 2,
                    sessionId: 'a9VAm22',


                    userCountry: 'AE',
                    userId: 'um29ava-mv92na-kieyqn-annv1',
                    userIp: '172.165.32.1',
                }
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });
});



describe('Test Recommend feedback Request', () => {

    test('recommend feedback', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.feedback.recommend.single({
            documentFeedback: {
                sourceId: 1,
                targetId: 2
            },
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response).toBeNull();
    });


    test('recommend batch feedback', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.feedback.recommend.batch({
            documentsFeedbacks: [{
                sourceId: 1,
                targetId: 2
            }],
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response).toBeNull();
    });

});