import { LablebHttpClient } from "../../http-client";
import { BatchRecommendFeedbackRequestParams, SingleRecommendFeedbackRequestParams } from "../../request/recommend-feedback/recommend-feedback.type";
import { camelCaseToSnackCaseObject } from "../../utils";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { RecommendFeedbackResponse } from "./recommend-feedback.type";


export async function lablebClientSingleRecommendFeedback(

    this: LablebSDKContext,

    singleRecommendFeedbackOptions: SingleRecommendFeedbackRequestParams

): Promise<LablebAPIResponseWrapper<RecommendFeedbackResponse>> {

    const { url, headers, method, params, body } = await this.requestBuilder.feedback.recommend.single(singleRecommendFeedbackOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
        body: body.map(camelCaseToSnackCaseObject),
    });
}


export async function lablebClientBatchRecommendFeedback(

    this: LablebSDKContext,

    batchRecommendFeedbackOptions: BatchRecommendFeedbackRequestParams

): Promise<LablebAPIResponseWrapper<RecommendFeedbackResponse>> {

    const { url, headers, method, params, body } = await this.requestBuilder.feedback.recommend.batch(batchRecommendFeedbackOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
        body: body.map(camelCaseToSnackCaseObject),
    });
}
