import { LablebHttpClient } from "../../http-client";
import { BatchAutocompleteFeedbackRequestParams, SingleAutocompleteFeedbackRequestParams } from "../../request/autocomplete-feedback/autocomplete-feedback.type";
import { camelCaseToSnackCaseObject } from "../../utils";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { AutocompleteFeedbackResponse } from "./autocomplete-feedback.type";



export async function lablebClientSingleAutocompleteFeedback(

    this: LablebSDKContext,

    singleAutocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams

): Promise<LablebAPIResponseWrapper<AutocompleteFeedbackResponse>> {

    const { url, headers, method, params, body } = await this.requestBuilder.feedback.autocomplete.single(singleAutocompleteFeedbackOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
        body: body.map(camelCaseToSnackCaseObject),
    });
}




export async function lablebClientBatchAutocompleteFeedback(

    this: LablebSDKContext,

    batchAutocompleteFeedbackOptions: BatchAutocompleteFeedbackRequestParams

): Promise<LablebAPIResponseWrapper<AutocompleteFeedbackResponse>> {

    const { url, headers, method, params, body } = await this.requestBuilder.feedback.autocomplete.batch(batchAutocompleteFeedbackOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
        body: body.map(camelCaseToSnackCaseObject),
    });
}
