import { LablebClient } from '../..';



describe('Test Auth', () => {

    test('Autocomplete feedback at lableb cloud without jwt', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                jwtToken: 'bad jwt'
            });

            await lablebClient.feedback.autocomplete.single({
                documentFeedback: {
                    query: 'a',
                }
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });


    test('Autocomplete feedback at lableb cloud without api key', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                APIKey: 'bad api key'
            });

            await lablebClient.feedback.autocomplete.single({
                documentFeedback: {
                    query: 'a',
                }
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });
});



describe('Test autocomplete feedback Request', () => {


    test('autocomplete feedback', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.feedback.autocomplete.single({
            documentFeedback: {
                query: 'hello',
                itemId: 1,
            },
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response).toBeNull();
    });


    test('autocomplete batch feedback', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.feedback.autocomplete.batch({
            documentsFeedbacks: [
                {
                    query: 'hello',
                    itemId: 1,
                    
                }
            ],
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response).toBeNull();
    });

});