import { LablebHttpClient } from "../../http-client";
import { RecommendRequestParams } from "../../request/recommend/recommend.type";
import { camelCaseToSnackCaseObject, customIdentity, customPickBy } from "../../utils";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { RecommendResponse, RecommendResponseWithFeedback } from "./recommend.type";



export async function lablebClientRecommend(

    this: LablebSDKContext,

    recommendOptions: RecommendRequestParams

): Promise<LablebAPIResponseWrapper<RecommendResponseWithFeedback>> {

    return injectFeedbackDataIntoRecommendResponse(
        recommendOptions,
        await lablebClientNativeRecommend.bind(this)(recommendOptions)
    );
}




async function lablebClientNativeRecommend(

    this: LablebSDKContext,

    recommendOptions: RecommendRequestParams

): Promise<LablebAPIResponseWrapper<RecommendResponse>> {

    const { url, headers, method, params } = await this.requestBuilder.recommend(recommendOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
    });
}





function injectFeedbackDataIntoRecommendResponse(

    recommendOptions: RecommendRequestParams,

    recommendResponse: LablebAPIResponseWrapper<RecommendResponse>

): LablebAPIResponseWrapper<RecommendResponseWithFeedback> {

    return {
        ...recommendResponse,
        response: {
            ...recommendResponse.response,
            results: recommendResponse.response.results.map((document, index) => ({
                ...document,
                feedback: {
                    sourceId: recommendOptions.id,
                    targetId: document.id,

                    sourceTitle: recommendOptions.title,
                    targetTitle: document.title,

                    sourceUrl: recommendOptions.url,
                    targetUrl: document.url,

                    itemOrder: index,
                    sessionId: recommendOptions.sessionId,
                    userCountry: recommendOptions.userCountry,
                    userId: recommendOptions.userId,
                    userIp: recommendOptions.userIp,
                },
            }))
        }
    }
}
