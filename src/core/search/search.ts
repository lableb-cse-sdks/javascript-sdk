import { LablebHttpClient } from "../../http-client";
import { SearchRequestParams } from "../../request/search/search.type";
import { camelCaseToSnackCaseObject, customIdentity, customPickBy } from "../../utils";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { SearchResponse, SearchResponseWithFeedback } from "./search.type";



export async function lablebClientSearch(

    this: LablebSDKContext,

    searchOptions: SearchRequestParams

): Promise<LablebAPIResponseWrapper<SearchResponseWithFeedback>> {

    return injectFeedbackDataIntoSearchResponse(
        searchOptions,
        await lablebClientNativeSearch.bind(this)(searchOptions)
    );
}



async function lablebClientNativeSearch(

    this: LablebSDKContext,

    searchOptions: SearchRequestParams

): Promise<LablebAPIResponseWrapper<SearchResponse>> {

    const { url, headers, method, params } = await this.requestBuilder.search(searchOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
    });
}





function injectFeedbackDataIntoSearchResponse(

    searchOptions: SearchRequestParams,

    searchResponse: LablebAPIResponseWrapper<SearchResponse>

): LablebAPIResponseWrapper<SearchResponseWithFeedback> {

    return {
        ...searchResponse,
        response: {
            ...searchResponse.response,
            results: searchResponse.response.results.map((document, index) => ({
                ...document,
                feedback: customPickBy({
                    query: searchOptions.query,
                    itemId: document.id,
                    itemOrder: index,
                    sessionId: searchOptions.sessionId,
                    url: document.url,
                    userCountry: searchOptions.userCountry,
                    userId: searchOptions.userId,
                    userIp: searchOptions.userIp,
                }, customIdentity),
            }))
        }
    }
}
