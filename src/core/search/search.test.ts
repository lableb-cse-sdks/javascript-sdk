import { MESSAGES } from "../../config/messages";
import { LablebClient } from "../lableb-client/lableb-client";


describe('Test Auth', () => {

    test('Search at lableb cloud without jwt', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                jwtToken: 'bad jwt'
            });

            await lablebClient.search({
                query: '*',
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });


    test('Search at lableb cloud without api key', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                APIKey: 'bad api key'
            });

            await lablebClient.search({
                query: '*',
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });
});



describe('Test Search Request', () => {


    test('Search at lableb cloud', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.search({
            query: '*',
        });


        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');

        if (response.results.length) {
            expect(response.results[0]).toHaveProperty('feedback');
            expect(response.results[0].feedback.query).toBe('*');
            expect(response.results[0].feedback.itemId).toBe(response.results[0].id);
        }
    });


    test('Search at lableb cloud where options passed to inner search', async () => {

        const lablebClient = await LablebClient();

        const { code, response, time } = await lablebClient.search({
            query: 'hello',
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');
    });



    test('Search at lableb cloud with all optional options', async () => {

        const lablebClient = await LablebClient({
            jwtToken: process.env.API_KEY,
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.search({
            query: 'hello',
            filter: 'title:nice',
            limit: 2,
            sessionId: '92492mfa',
            skip: 0,
            sort: 'title asc',
            userId: '42',
            userIp: '172.65.23.22',
            userCountry: 'AE',
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');
    });

    // @todo add feedback

});





describe('Bad Inputs', () => {

    test('Search at lableb cloud with no query', async () => {
        try {
            const lablebClient = await LablebClient({
                APIKey: process.env.API_KEY,
                platformName: process.env.PLATFORM_NAME,
            });

            await lablebClient.search({
                query: '',
            });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.QUERY_IS_REQUIRED);
        }
    });

});