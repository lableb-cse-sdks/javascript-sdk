import { LablebClient } from "../lableb-client/lableb-client";




describe('Test Auth', () => {

    test('Search feedback at lableb cloud without jwt', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                jwtToken: 'bad jwt'
            });

            await lablebClient.feedback.search.single({
                documentFeedback: {
                    query: 'a'
                }
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });


    test('Search feedback at lableb cloud without api key', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                APIKey: 'bad api key'
            });

            await lablebClient.feedback.search.single({
                documentFeedback: {
                    query: 'a',
                },
                APIKey: '',
                indexName: '',
                jwtToken: '',
                platformName: '',
                searchHandler: ''
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });
});



describe('Test Search feedback Request', () => {

    test('search feedback', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.feedback.search.single({
            documentFeedback: {
                query: 'hello',
                itemId: 1,
            },
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response).toBeNull();

    });



    test('search batch feedback', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.feedback.search.batch({
            documentsFeedbacks: [{
                query: 'hello',
                itemId: 1,
            }],
        });

        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response).toBeNull();
    });




    test('Search at lableb cloud then feedback it', async () => {

        const lablebClient = await LablebClient({
            APIKey: process.env.API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const { code, response, time } = await lablebClient.search({
            query: '*',
        });


        expect(code).toBe(200);
        expect(time).toBeGreaterThan(0);
        expect(response.results.length).toBeGreaterThanOrEqual(0);
        expect(response.found_documents).toBeGreaterThanOrEqual(0);
        expect(typeof response.facets).toEqual('object');

        if (response.results.length) {

            expect(response.results[0]).toHaveProperty('feedback');
            expect(response.results[0].feedback.query).toBe('*');
            expect(response.results[0].feedback.itemId).toBe(response.results[0].id);


            const feedbackResponse = await lablebClient.feedback.search.single({
                documentFeedback: response.results[0].feedback
            });

            expect(feedbackResponse.code).toBe(200);
            expect(feedbackResponse.time).toBeGreaterThan(0);
            expect(feedbackResponse.response).toBeNull();
        }
    });

});