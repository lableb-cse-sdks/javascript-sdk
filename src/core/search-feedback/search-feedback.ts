import { LablebHttpClient } from "../../http-client";
import { BatchSearchFeedbackRequestParams, SingleSearchFeedbackRequestParams } from "../../request/search-feedback/search-feedback.type";
import { camelCaseToSnackCaseObject } from "../../utils";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { SearchFeedbackResponse } from "./search-feedback.type";


export async function lablebClientSingleSearchFeedback(

    this: LablebSDKContext,

    singleSearchFeedbackOptions: SingleSearchFeedbackRequestParams

): Promise<LablebAPIResponseWrapper<SearchFeedbackResponse>> {

    const { url, headers, method, params, body } = await this.requestBuilder.feedback.search.single(singleSearchFeedbackOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
        body: body.map(camelCaseToSnackCaseObject),
    });
}


export async function lablebClientBatchSearchFeedback(

    this: LablebSDKContext,

    batchSearchFeedbackOptions: BatchSearchFeedbackRequestParams

): Promise<LablebAPIResponseWrapper<SearchFeedbackResponse>> {

    const { url, headers, method, params, body } = await this.requestBuilder.feedback.search.batch(batchSearchFeedbackOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
        body: body.map(camelCaseToSnackCaseObject),
    });
}
