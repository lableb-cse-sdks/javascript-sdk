import { LablebHttpClient } from "../../http-client";
import { IndexingRequestParams } from "../../request/indexing/indexing.type";
import { camelCaseToSnackCaseObject } from "../../utils";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { IndexingResponse } from "./indexing.type";


export async function lablebClientIndexing(

    this: LablebSDKContext,

    indexingOptions: IndexingRequestParams

): Promise<LablebAPIResponseWrapper<IndexingResponse>> {

    const { url, headers, method, params, body } = await this.requestBuilder.index(indexingOptions);


    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
        body: body.map(camelCaseToSnackCaseObject),
    });
}

