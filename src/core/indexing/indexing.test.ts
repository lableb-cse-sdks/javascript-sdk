import { generate } from 'randomstring';
import { MESSAGES } from '../../config/messages';
import { LablebClient } from '../lableb-client/lableb-client';
import { LablebDocumentInput } from '../lableb-client/lableb-client.type';




describe('Test Auth', () => {

    test('index at lableb cloud without jwt', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                jwtToken: 'bad jwt'
            });

            await lablebClient.index({
                documents: [{ id: '1' }]
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });


    test('index at lableb cloud without api key', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                indexingAPIKey: 'bad api key'
            });

            await lablebClient.index({
                documents: [{ id: '1' }]
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });
});




describe('Test index Request', () => {


    test('index documents at lableb cloud', async () => {

        const lablebClient = await LablebClient({
            indexingAPIKey: process.env.INDEX_API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const DOCUMENTS: LablebDocumentInput[] = [
            {
                id: 1,
                url: `https://example.com/${generate()}`,
                title: generate(),
            },
            {
                id: 2,
                url: `https://example.com/${generate()}`,
                title: generate(),
            },
            {
                id: 3,
                url: `https://example.com/${generate()}`,
                title: 'Hello Lableb',
                tags: ['lableb', 'greeting', 'test']
            },
            {
                id: 4,
                url: `https://example.com/${generate()}`,
                title: 'Hello World',
                tags: ['greeting', 'the world']
            },
        ]



        const indexResult = await lablebClient.index({
            documents: DOCUMENTS,
        });

        expect(indexResult.code).toBe(200);
        expect(indexResult.time).toBeGreaterThan(0);
        expect(indexResult.response).toBeNull();

    });


});



describe('Bad Inputs', () => {

    test('index no documents at lableb cloud', async () => {
        try {
            const lablebClient = await LablebClient({
                indexingAPIKey: process.env.INDEX_API_KEY,
                platformName: process.env.PLATFORM_NAME,
            });

            await lablebClient.index({} as any);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.DOCUMENTS_IS_REQUIRED);
        }
    });




    test('index empty documents at lableb cloud', async () => {
        try {
            const lablebClient = await LablebClient({
                indexingAPIKey: process.env.INDEX_API_KEY,
                platformName: process.env.PLATFORM_NAME,
            });

            await lablebClient.index({ documents: [] });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.DOCUMENTS_LENGTH_IS_INVALID);
        }
    });


    test('index documents at lableb cloud without index api key', async () => {
        try {
            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
            });

            await lablebClient.index({ documents: [{ id: 3 }] });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.INDEXING_API_KEY_IS_REQUIRED);
        }
    });


});


