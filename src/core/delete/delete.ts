import { LablebHttpClient } from "../../http-client";
import { DeleteRequestParams } from "../../request/delete/delete.type";
import { camelCaseToSnackCaseObject } from "../../utils";
import { LablebAPIResponseWrapper, LablebSDKContext } from "../lableb-client/lableb-client.type";
import { DeleteResponse } from "./delete.type";


export async function lablebClientDelete(

    this: LablebSDKContext,

    deleteOptions: DeleteRequestParams

): Promise<LablebAPIResponseWrapper<DeleteResponse>> {

    const { url, headers, method, params } = await this.requestBuilder.delete(deleteOptions);

    return await LablebHttpClient({
        method,
        url,
        headers,
        params: camelCaseToSnackCaseObject(params),
    });
}

