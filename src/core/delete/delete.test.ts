import { generate } from 'randomstring';
import { MESSAGES } from '../../config/messages';
import { LablebClient } from '../lableb-client/lableb-client';
import { LablebDocumentInput } from '../lableb-client/lableb-client.type';



describe('Test Auth', () => {

    test('delete at lableb cloud without jwt', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                jwtToken: 'bad jwt'
            });

            await lablebClient.delete({
                documentId: 1,
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });


    test('delete at lableb cloud without api key', async () => {

        try {

            const lablebClient = await LablebClient({
                platformName: process.env.PLATFORM_NAME,
                indexingAPIKey: 'bad api key'
            });

            await lablebClient.delete({
                documentId: 1,
            });

        } catch (error) {
            expect(error.message).toContain(401);
        }
    });
});

describe('Test Delete Request', () => {



    test('delete document at lableb cloud', async () => {

        const lablebClient = await LablebClient({
            indexingAPIKey: process.env.INDEX_API_KEY,
            platformName: process.env.PLATFORM_NAME,
        });

        const DOCUMENTS: LablebDocumentInput[] = [
            {
                id: 1,
                url: `https://example.com/${generate()}`,
                title: generate(),
            },
        ]

        const indexResult = await lablebClient.index({
            documents: DOCUMENTS,
        });

        expect(indexResult.code).toBe(200);
        expect(indexResult.time).toBeGreaterThan(0);
        expect(indexResult.response).toBeNull();

        const deleteResult = await lablebClient.delete({
            documentId: 1,
        });

        expect(deleteResult.code).toBe(200);
        expect(deleteResult.time).toBeGreaterThan(0);
        expect(deleteResult.response).toBeNull();

    });

});


describe('Bad Inputs', () => {

    test('delete no documents at lableb cloud', async () => {
        try {
            const lablebClient = await LablebClient({
                indexingAPIKey: process.env.INDEX_API_KEY,
                platformName: process.env.PLATFORM_NAME,
            });

            await lablebClient.delete({} as any);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.DOCUMENT_ID_IS_REQUIRED);
        }
    });
});
