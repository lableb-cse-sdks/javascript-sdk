import { v4 as generateUUID } from 'uuid';
import { generate } from "randomstring";
import { LablebRequestBuilder } from '../main/main.request';
import { IndexingRequestParams } from './indexing.type';
import { MESSAGES } from '../../config/messages';
import { GlobalRequestOptions, LablebRequestBuilderType } from '../main/main.request.type';



describe('Missing required fields', () => {

    test('missing platform name', async () => {

        try {

            const lablebRequest = await LablebRequestBuilder();

            const indexingOptions: IndexingRequestParams = {
                documents: [{ id: 34 }],
                indexName: generate(),
                indexingAPIKey: generateUUID(),
            }

            await lablebRequest.index(indexingOptions);

            throw new Error('unexpected error');

        } catch (error) {

            expect(error.message).toEqual(MESSAGES.PLATFORM_NAME_IS_REQUIRED)
        }
    });



    test('missing documents', async () => {

        try {

            const lablebRequest = await LablebRequestBuilder();

            const indexingOptions: any = {
                platformName: generate(),
                indexName: generate(),
                indexingAPIKey: generateUUID(),
            }

            await lablebRequest.index(indexingOptions);

            throw new Error('unexpected error');

        } catch (error) {

            expect(error.message).toEqual(MESSAGES.DOCUMENTS_IS_REQUIRED)
        }
    });


    test('wrong documents length', async () => {

        try {

            const lablebRequest = await LablebRequestBuilder();

            const indexingOptions: IndexingRequestParams = {
                documents: [],
                platformName: generate(),
                indexName: generate(),
                indexingAPIKey: generateUUID(),
            }

            await lablebRequest.index(indexingOptions);

            throw new Error('unexpected error');

        } catch (error) {

            expect(error.message).toEqual(MESSAGES.DOCUMENTS_LENGTH_IS_INVALID)
        }
    });



    test('missing index api key or jwt token', async () => {

        try {

            const lablebRequest = await LablebRequestBuilder();

            const indexingOptions: IndexingRequestParams = {
                documents: [{ id: 35 }],
                platformName: generate(),
                indexName: generate(),
            }

            await lablebRequest.index(indexingOptions);

            throw new Error('unexpected error');

        } catch (error) {

            expect([
                MESSAGES.JWT_TOKEN_IS_REQUIRED,
                MESSAGES.INDEXING_API_KEY_IS_REQUIRED
            ]).toContain(error.message);
        }
    });


});


/*************************/


describe('Pass required fields and compare', () => {

    test('compare all required fields', async () => {

        const lablebRequest = await LablebRequestBuilder();

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            platformName: generate(),
            indexName: generate(),
            indexingAPIKey: generateUUID(),
        }

        const { url, method, body, headers, params } = await lablebRequest.index(indexingOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${indexingOptions.platformName}/indices/${indexingOptions.indexName}/documents`;

        expect(url).toEqual(expectedURL);

        expect(method).toEqual('POST');

        expect(params.apikey).toEqual(indexingOptions.indexingAPIKey);

        expect(body).toEqual(indexingOptions.documents.map(doc => ({ ...doc, id: String(doc.id) })));
        expect(body).toHaveLength(1);
    });

});


/*************************/


describe('Pass optional fields and compare', () => {

    test('compare all optional fields', async () => {

        const lablebRequest = await LablebRequestBuilder();

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            platformName: generate(),
            indexName: generate(),
            indexingAPIKey: generateUUID(),
            jwtToken: generateUUID(),
        }

        const { headers } = await lablebRequest.index(indexingOptions);
        expect(headers['Authorization']).toEqual(`Bearer ${indexingOptions.jwtToken}`);

    });

});




describe('Test global/private options', () => {

    const globalOptions: GlobalRequestOptions = {
        platformName: generate(),
        indexName: generate(),
        indexingAPIKey: generateUUID(),
        jwtToken: generateUUID(),
    }

    let lablebRequest: LablebRequestBuilderType;

    beforeAll(async () => {
        lablebRequest = await LablebRequestBuilder(globalOptions);;
    });


    test('using global platform name', async () => {

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            indexName: generate(),
            indexingAPIKey: generateUUID(),
        }

        const { url } = await lablebRequest.index(indexingOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${indexingOptions.indexName}/documents`;

        expect(url).toEqual(expectedURL);
    });



    test('using private platform name', async () => {

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            indexName: generate(),
            indexingAPIKey: generateUUID(),
            platformName: generate(),
        }

        const { url } = await lablebRequest.index(indexingOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${indexingOptions.platformName}/indices/${indexingOptions.indexName}/documents`;

        expect(url).toEqual(expectedURL);
    });


    test('using global index name', async () => {

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            indexingAPIKey: generateUUID(),
            platformName: generate(),
        }

        const { url } = await lablebRequest.index(indexingOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${indexingOptions.platformName}/indices/${globalOptions.indexName}/documents`;

        expect(url).toEqual(expectedURL);
    });


    test('using private index name', async () => {

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            indexingAPIKey: generateUUID(),
            platformName: generate(),
            indexName: generate(),
        }

        const { url } = await lablebRequest.index(indexingOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${indexingOptions.platformName}/indices/${indexingOptions.indexName}/documents`;

        expect(url).toEqual(expectedURL);
    });


    test('using global indexing api key', async () => {

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            indexName: generate(),
            platformName: generate(),
        }

        const { params } = await lablebRequest.index(indexingOptions);

        expect(params.apikey).toEqual(globalOptions.indexingAPIKey);
    });

    test('using private indexing api key', async () => {

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            indexName: generate(),
            platformName: generate(),
            indexingAPIKey: generateUUID(),
        }

        const { params } = await lablebRequest.index(indexingOptions);

        expect(params.apikey).toEqual(indexingOptions.indexingAPIKey);
    });


    test('using global jwt token', async () => {

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            indexName: generate(),
            indexingAPIKey: generateUUID(),
            platformName: generate(),
        }

        const { headers } = await lablebRequest.index(indexingOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${globalOptions.jwtToken}`)
    });


    test('using private jwt token', async () => {

        const indexingOptions: IndexingRequestParams = {
            documents: [{ id: 34 }],
            indexName: generate(),
            jwtToken: generateUUID(),
            platformName: generate(),
        }

        const { headers } = await lablebRequest.index(indexingOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${indexingOptions.jwtToken}`)
    });

});



describe('Validate bad inputs', () => {


    test('not passing an id in documents', async () => {

        try {

            const indexingOptions: IndexingRequestParams = {
                documents: [{ text: generate() }] as any,
                indexName: generate(),
                jwtToken: generateUUID(),
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.index(indexingOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.DOCUMENT_ID_IS_REQUIRED);
        }
    });


    test('passing id as float number', async () => {

        try {

            const indexingOptions: IndexingRequestParams = {
                documents: [{ id: 42.52 }] as any,
                indexName: generate(),
                jwtToken: generateUUID(),
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.index(indexingOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.IS_INVALID_DOCUMENT_ID);
        }
    });
});