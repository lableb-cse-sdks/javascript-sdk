import * as yup from 'yup';
import { isValidDocumentIdForYup } from '../../../src/utils';
import { MESSAGES } from '../../config/messages';
import { LablebDocument } from '../../core/lableb-client/lableb-client.type';
import { IndexingRequestParams } from './indexing.type';


interface IndexingSchema extends Omit<IndexingRequestParams, "documents"> {
    /**
     * the optional key/value in LablebAPIDocument caused issue for yup
     * so we define it as an object which only contains "id" field 
     */
    documents: (Pick<LablebDocument, "id">)[],
}


export const buildIndexingRequestSchema: yup.SchemaOf<IndexingSchema> =
    yup.object()
        .shape({

            indexingAPIKey: yup
                .string()
                .test('test-if-can-authenticate',
                    MESSAGES.INDEXING_API_KEY_IS_REQUIRED,
                    function testIfCanAuthenticate(value, context) {

                        if (!value && !context.parent.jwtToken)
                            return false;

                        return true;
                    }),


            jwtToken: yup.string().optional(),


            platformName: yup.string().required(MESSAGES.PLATFORM_NAME_IS_REQUIRED),
            indexName: yup.string().required(MESSAGES.INDEX_NAME_IS_REQUIRED),

            documents: yup
                .array()
                .required(MESSAGES.DOCUMENTS_IS_REQUIRED)
                .min(1, MESSAGES.DOCUMENTS_LENGTH_IS_INVALID)
                .of(yup.object().shape({
                    id: yup.string().required(MESSAGES.DOCUMENT_ID_IS_REQUIRED).test('test-id', MESSAGES.IS_INVALID_DOCUMENT_ID, isValidDocumentIdForYup),
                })),

        },
            // to allow circular dependency
            [['jwtToken', 'indexingAPIKey']]
        );
