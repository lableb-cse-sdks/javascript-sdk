import { LablebDocument } from '../../core/lableb-client/lableb-client.type';
import { appendJwtTokenHeader, customIdentity, customPickBy } from '../../utils';
import { GlobalRequestOptions } from '../main/main.request.type';
import { buildIndexingRequestSchema } from './indexing.schema';
import { IndexingRequestParams, IndexingRequestResult } from './indexing.type';


export async function buildIndexingRequest(this: GlobalRequestOptions, params: IndexingRequestParams): Promise<IndexingRequestResult> {


    const validatedParams = await buildIndexingRequestSchema.validate({
        indexName: params.indexName || this?.indexName || process.env.GLOBAL_DEFAULT_INDEX_NAME,
        platformName: params.platformName || this?.platformName,
        indexingAPIKey: params.indexingAPIKey || this?.indexingAPIKey,
        jwtToken: params.jwtToken || this?.jwtToken,
        documents: params.documents,
    });


    const {
        indexName,
        platformName,
        indexingAPIKey,
        jwtToken,
        documents,
    } = validatedParams;



    return {
        method: 'POST',
        url: `${process.env.API_BASE_URL}/projects/${platformName}/indices/${indexName}/documents`,
        params: customPickBy(
            {
                apikey: indexingAPIKey,
            },
            customIdentity
        ),
        headers: appendJwtTokenHeader(jwtToken, {}),
        body: documents as LablebDocument[],
    }
}