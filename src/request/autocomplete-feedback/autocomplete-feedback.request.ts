import { appendJwtTokenHeader, customIdentity, customPickBy } from '../../utils';
import { FeedbackDocument } from '../feedback/feedback.document.type';
import { GlobalRequestOptions } from '../main/main.request.type';
import { buildBatchAutocompleteFeedbackRequestSchema } from './autocomplete-feedback.schema';
import { BatchAutocompleteFeedbackRequestParams, BatchAutocompleteFeedbackRequestResult, SingleAutocompleteFeedbackRequestParams } from './autocomplete-feedback.type';


export async function buildSingleAutocompleteFeedbackRequest(this: GlobalRequestOptions, params: SingleAutocompleteFeedbackRequestParams): Promise<BatchAutocompleteFeedbackRequestResult> {

    const { documentFeedback, ...rest } = params;

    const options: any = {
        ...rest,
    }

    if (documentFeedback) {
        options.documentsFeedbacks = [documentFeedback];
    }

    return buildBatchAutocompleteFeedbackRequest.bind(this)(options);
}


export async function buildBatchAutocompleteFeedbackRequest(this: GlobalRequestOptions, params: BatchAutocompleteFeedbackRequestParams): Promise<BatchAutocompleteFeedbackRequestResult> {


    const validatedParams = await buildBatchAutocompleteFeedbackRequestSchema.validate({
        ...params,
        indexName: params.indexName || this?.indexName || process.env.GLOBAL_DEFAULT_INDEX_NAME,
        platformName: params.platformName || this?.platformName,
        autocompleteHandler: params.autocompleteHandler || this?.autocompleteHandler || process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER,
        APIKey: params.APIKey || this?.APIKey,
        jwtToken: params.jwtToken || this?.jwtToken,
        documentsFeedbacks: params.documentsFeedbacks,
    });


    const {
        indexName,
        platformName,
        autocompleteHandler,
        APIKey,
        jwtToken,
        documentsFeedbacks,
    } = validatedParams;


    return {
        method: 'POST',
        url: `${process.env.API_BASE_URL}/projects/${platformName}/indices/${indexName}/autocomplete/${autocompleteHandler}/feedback/hits`,
        params: customPickBy(
            {
                apikey: APIKey,
            },
            customIdentity
        ),
        headers: appendJwtTokenHeader(jwtToken, {}),
        body: documentsFeedbacks as FeedbackDocument[],
    }
}

