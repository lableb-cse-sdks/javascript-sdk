import { v4 as generateUUID } from 'uuid';
import { generate } from "randomstring";
import { BatchAutocompleteFeedbackRequestParams, SingleAutocompleteFeedbackRequestParams } from './autocomplete-feedback.type';
import { LablebRequestBuilder } from '../main/main.request';
import { MESSAGES } from '../../config/messages';
import { GlobalRequestOptions, LablebRequestBuilderType } from '../main/main.request.type';




describe('Missing required fields', () => {

    test('missing feedback documents', async () => {
        try {

            const autocompleteFeedbackOptions: any = {
                APIKey: generateUUID(),
                platformName: generate(),
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);


        } catch (error) {
            expect(error.message).toEqual(MESSAGES.FEEDBACK_DOCUMENTS_ARE_REQUIRED)
        }
    });


    test('missing feedback documents in batch', async () => {
        try {

            const autocompleteFeedbackOptions: any = {
                APIKey: generateUUID(),
                platformName: generate(),
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.batch(autocompleteFeedbackOptions);


        } catch (error) {
            expect(error.message).toEqual(MESSAGES.FEEDBACK_DOCUMENTS_ARE_REQUIRED)
        }
    });



    test('missing platform name', async () => {
        try {

            const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
                APIKey: generateUUID(),
                documentFeedback: {
                    query: generate(),
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);


        } catch (error) {
            expect(error.message).toEqual(MESSAGES.PLATFORM_NAME_IS_REQUIRED)
        }
    });


    test('missing platform name in batch', async () => {
        try {

            const autocompleteFeedbackOptions: BatchAutocompleteFeedbackRequestParams = {
                APIKey: generateUUID(),
                documentsFeedbacks: [{ query: generate() }],
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.batch(autocompleteFeedbackOptions);


        } catch (error) {
            expect(error.message).toEqual(MESSAGES.PLATFORM_NAME_IS_REQUIRED)
        }
    });




    test('missing api key', async () => {
        try {

            const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
                platformName: generateUUID(),
                documentFeedback: {
                    query: generate(),
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);


        } catch (error) {
            expect([
                MESSAGES.API_KEY_IS_REQUIRED,
                MESSAGES.JWT_TOKEN_IS_REQUIRED,
            ]).toContain(error.message);
        }
    });


    test('missing api key in batch', async () => {
        try {

            const autocompleteFeedbackOptions: BatchAutocompleteFeedbackRequestParams = {
                platformName: generateUUID(),
                documentsFeedbacks: [{ query: generate() }],
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.batch(autocompleteFeedbackOptions);


        } catch (error) {
            expect([
                MESSAGES.API_KEY_IS_REQUIRED,
                MESSAGES.JWT_TOKEN_IS_REQUIRED,
            ]).toContain(error.message);
        }
    });

});

describe('Passing required fields and compare', () => {

    test('passing only required fields', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            platformName: generate(),
            APIKey: generateUUID(),
            documentFeedback: {
                query: generate(),
            }
        };

        const lablebRequest = await LablebRequestBuilder();
        const { url, method, headers, params, body } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${autocompleteFeedbackOptions.platformName}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/autocomplete/${process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER}/feedback/hits`;

        expect(expectedURL).toEqual(url);
        expect(method).toEqual('POST');

        expect(params.apikey).toEqual(autocompleteFeedbackOptions.APIKey);

        expect(body).toEqual([autocompleteFeedbackOptions.documentFeedback]);
    });
});


describe('Passing optional fields and compare', () => {


    test('passing all optional fields', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            platformName: generate(),
            APIKey: generateUUID(),
            indexName: generate(),
            autocompleteHandler: generate(),
            jwtToken: generateUUID(),
            documentFeedback: {
                query: generate(),
                itemId: 3,
                itemOrder: 5,
                sessionId: generate(),
                url: 'https://www.example.com',
                userCountry: 'AF',
                userId: '42',
                userIp: '192.168.1.1'
            }
        };

        const lablebRequest = await LablebRequestBuilder();
        const { url, method, headers, params, body } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${autocompleteFeedbackOptions.platformName}/indices/${autocompleteFeedbackOptions.indexName}/autocomplete/${autocompleteFeedbackOptions.autocompleteHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
        expect(method).toEqual('POST');

        expect(headers['Authorization']).toEqual(`Bearer ${autocompleteFeedbackOptions.jwtToken}`);

        expect(params.apikey).toEqual(autocompleteFeedbackOptions.APIKey);

        expect(body).toEqual([{
            ...autocompleteFeedbackOptions.documentFeedback,
            itemId: String(autocompleteFeedbackOptions.documentFeedback.itemId),
        }]);
    });
});




describe('passing public/private options', () => {

    const globalOptions: GlobalRequestOptions = {
        indexName: generate(),
        APIKey: generateUUID(),
        autocompleteHandler: generate(),
        jwtToken: generateUUID(),
        platformName: generate(),
    }

    let lablebRequest: LablebRequestBuilderType;

    beforeAll(async () => {
        lablebRequest = await LablebRequestBuilder(globalOptions);
    });

    test('using public index name', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            APIKey: generateUUID(),
            documentFeedback: {
                query: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${globalOptions.indexName}/autocomplete/${globalOptions.autocompleteHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using private index name', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            indexName: generate(),
            APIKey: generateUUID(),
            documentFeedback: {
                query: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${autocompleteFeedbackOptions.indexName}/autocomplete/${globalOptions.autocompleteHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using public platform name', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            APIKey: generateUUID(),
            documentFeedback: {
                query: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${globalOptions.indexName}/autocomplete/${globalOptions.autocompleteHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using private platform name', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            platformName: generate(),
            indexName: generate(),
            APIKey: generateUUID(),
            documentFeedback: {
                query: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${autocompleteFeedbackOptions.platformName}/indices/${autocompleteFeedbackOptions.indexName}/autocomplete/${globalOptions.autocompleteHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using public autocomplete handler', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            APIKey: generateUUID(),
            documentFeedback: {
                query: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${globalOptions.indexName}/autocomplete/${globalOptions.autocompleteHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using private autocomplete handler', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            autocompleteHandler: generate(),
            APIKey: generateUUID(),
            documentFeedback: {
                query: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${globalOptions.indexName}/autocomplete/${autocompleteFeedbackOptions.autocompleteHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using public jwt', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            documentFeedback: {
                query: generate(),
            }
        };

        const { headers } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);


        expect(headers['Authorization']).toEqual(`Bearer ${globalOptions.jwtToken}`);
    });


    test('using private jwt', async () => {

        const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
            jwtToken: generateUUID(),
            documentFeedback: {
                query: generate(),
            }
        };

        const { headers } = await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${autocompleteFeedbackOptions.jwtToken}`);
    });

});




describe('Validate bad inputs', () => {

    test('invalid document id', async () => {
        try {
            const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                autocompleteHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    query: generate(),
                    itemId: 42.2,
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.IS_INVALID_DOCUMENT_ID)
        }
    });


    test('invalid document order', async () => {
        try {
            const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                autocompleteHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    query: generate(),
                    itemOrder: 5.2,
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.ITEM_ORDER_IS_NOT_INTEGER)
        }
    });


    test('invalid document url', async () => {
        try {
            const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                autocompleteHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    query: generate(),
                    url: 'https:www.example.com',
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.URL_IS_NOT_VALID)
        }
    });



    test('invalid user id', async () => {
        try {
            const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                autocompleteHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    query: generate(),
                    userId: true as any,
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.INVALID_USER_ID)
        }
    });


    test('invalid user ip', async () => {
        try {
            const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                autocompleteHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    query: generate(),
                    userIp: '455.168.1.1'
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.INVALID_IP_ADDRESS)
        }
    });



    test('invalid user country 1', async () => {
        try {
            const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                autocompleteHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    query: generate(),
                    userCountry: '',
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID)
        }
    });


    test('invalid user country 2', async () => {
        try {
            const autocompleteFeedbackOptions: SingleAutocompleteFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                autocompleteHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    query: generate(),
                    userCountry: 'VSX',
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.autocomplete.single(autocompleteFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID)
        }
    });
});