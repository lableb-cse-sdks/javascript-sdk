import * as yup from 'yup';
import { MESSAGES } from '../../config/messages';
import { isTruthyString } from '../../utils';
import { feedbackDocumentSchema } from '../feedback/feedback.document.schema';
import { baseRequestSchema } from '../main/main.request.schema';
import { BatchAutocompleteFeedbackRequestParams } from './autocomplete-feedback.type';


export const buildBatchAutocompleteFeedbackRequestSchema: yup.SchemaOf<BatchAutocompleteFeedbackRequestParams> =
    baseRequestSchema.concat(
        yup.object()
            .shape({

                autocompleteHandler: yup.string().optional(),

                APIKey: yup
                    .string()
                    .test('test-if-can-authenticate',
                        MESSAGES.API_KEY_IS_REQUIRED,
                        function testIfCanAuthenticate(value, context) {

                            if (!value && !context.parent.jwtToken)
                                return false;

                            return true;
                        }),

                jwtToken: yup.string().optional(),

                documentsFeedbacks: yup.array()
                    .required(MESSAGES.FEEDBACK_DOCUMENTS_ARE_REQUIRED)
                    .min(1, MESSAGES.FEEDBACK_DOCUMENTS_LENGTH_INVALID)
                    .of(
                        feedbackDocumentSchema
                    ),


            },
                // to allow circular dependency
                [['jwtToken', 'APIKey']]
            )
    );
