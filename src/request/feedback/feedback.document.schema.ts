import * as yup from 'yup';
import { MESSAGES } from '../../config/messages';
import { isValidDocumentIdForYup, isValidIntegerForYup, isValidIPAddress } from '../../utils';
import { optionalBaseRequestSchema } from '../main/main.request.schema';
import { FeedbackDocument, RecommendFeedbackDocument } from './feedback.document.type';


export const feedbackDocumentSchema: yup.SchemaOf<FeedbackDocument> =
    optionalBaseRequestSchema.concat(
        yup.object().shape({

            query: yup.string().required(MESSAGES.QUERY_IS_REQUIRED),

            url: yup.string().url(MESSAGES.URL_IS_NOT_VALID).optional(),
            itemId: yup.string().optional().test('test-id', MESSAGES.IS_INVALID_DOCUMENT_ID, isValidDocumentIdForYup),

            itemOrder: yup.number().optional().test('is-integer', MESSAGES.ITEM_ORDER_IS_NOT_INTEGER, isValidIntegerForYup),
        })
    );



export const recommendFeedbackDocumentSchema: yup.SchemaOf<RecommendFeedbackDocument> =
    optionalBaseRequestSchema.concat(
        yup.object().shape({

            sourceId: yup.string().required(MESSAGES.SOURCE_ID_IS_REQUIRED).test('test-id', MESSAGES.SOURCE_ID_IS_INVALID_DOCUMENT_ID, isValidDocumentIdForYup),
            sourceTitle: yup.string().optional().min(1, MESSAGES.SOURCE_TITLE_LENGTH_IS_INVALID),
            sourceUrl: yup.string().optional().url(MESSAGES.URL_IS_NOT_VALID),

            targetId: yup
                .string()
                .required(MESSAGES.TARGET_ID_IS_REQUIRED)
                .test('test-id', MESSAGES.TARGET_ID_IS_INVALID_DOCUMENT_ID, isValidDocumentIdForYup)
                .test(
                    'test-equal-with-source-id',
                    MESSAGES.TARGET_ID_MUST_BE_DIFFERENT_THAN_SOURCE_ID,
                    function notEqualToSourceId(value, context) {

                        if (context.parent.sourceId == context.parent.targetId)
                            return false;

                        return true;
                    }),
            targetTitle: yup.string().optional().min(1, MESSAGES.TARGET_TITLE_LENGTH_IS_INVALID),
            targetUrl: yup.string().optional().url(MESSAGES.URL_IS_NOT_VALID),

            itemOrder: yup.number().optional().test('is-integer', MESSAGES.ITEM_ORDER_IS_NOT_INTEGER, isValidIntegerForYup),

        })
    );
