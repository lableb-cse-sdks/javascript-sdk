import { BaseRequestParams, BuildRequestResult } from "../../types";
import { RecommendFeedbackDocument } from "../feedback/feedback.document.type";


export interface BatchRecommendFeedbackRequestParams extends
    Omit<BaseRequestParams, "sessionId" | "userId" | "userIp" | "userCountry"> {

    APIKey?: string,

    recommendHandler?: string,

    documentsFeedbacks: RecommendFeedbackDocument[],
}

export interface SingleRecommendFeedbackRequestParams extends
    Omit<BatchRecommendFeedbackRequestParams, "documentsFeedbacks"> {

    documentFeedback: RecommendFeedbackDocument,
}


export interface BatchRecommendFeedbackRequestResult extends BuildRequestResult {

    params: {
        apikey?: string,
    },

    body: RecommendFeedbackDocument[],
}