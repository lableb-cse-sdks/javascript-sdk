import * as yup from 'yup';
import { MESSAGES } from '../../config/messages';
import { isTruthyString } from '../../utils';
import { recommendFeedbackDocumentSchema } from '../feedback/feedback.document.schema';
import { baseRequestSchema } from '../main/main.request.schema';
import { BatchRecommendFeedbackRequestParams } from './recommend-feedback.type';


export const buildBatchRecommendFeedbackRequestSchema: yup.SchemaOf<BatchRecommendFeedbackRequestParams> =
    baseRequestSchema.concat(
        yup.object()
            .shape({

                recommendHandler: yup.string().optional(),

                APIKey: yup
                    .string()
                    .test('test-if-can-authenticate',
                        MESSAGES.API_KEY_IS_REQUIRED,
                        function testIfCanAuthenticate(value, context) {

                            if (!value && !context.parent.jwtToken)
                                return false;

                            return true;
                        }),

                jwtToken: yup.string().optional(),

                documentsFeedbacks: yup.array()
                    .required(MESSAGES.FEEDBACK_DOCUMENTS_ARE_REQUIRED)
                    .min(1, MESSAGES.FEEDBACK_DOCUMENTS_LENGTH_INVALID)
                    .of(
                        recommendFeedbackDocumentSchema
                    ),


            },
                // to allow circular dependency
                [['jwtToken', 'APIKey']]
            )
    );
