import { LablebRequestBuilder } from '../main/main.request';
import { v4 as generateUUID } from 'uuid';
import { generate } from "randomstring";
import { MESSAGES } from '../../config/messages';
import { GlobalRequestOptions, LablebRequestBuilderType } from '../main/main.request.type';
import { BatchRecommendFeedbackRequestParams, SingleRecommendFeedbackRequestParams } from './recommend-feedback.type';


describe('Missing required fields', () => {

    test('missing feedback documents', async () => {
        try {

            const recommendFeedbackOptions: any = {
                APIKey: generateUUID(),
                platformName: generate(),
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);


        } catch (error) {
            expect(error.message).toEqual(MESSAGES.FEEDBACK_DOCUMENTS_ARE_REQUIRED)
        }
    });


    test('missing feedback documents in batch', async () => {
        try {

            const recommendFeedbackOptions: any = {
                APIKey: generateUUID(),
                platformName: generate(),
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.batch(recommendFeedbackOptions);


        } catch (error) {
            expect(error.message).toEqual(MESSAGES.FEEDBACK_DOCUMENTS_ARE_REQUIRED)
        }
    });



    test('missing platform name', async () => {
        try {

            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                APIKey: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);


        } catch (error) {
            expect(error.message).toEqual(MESSAGES.PLATFORM_NAME_IS_REQUIRED)
        }
    });


    test('missing platform name in batch', async () => {
        try {

            const recommendFeedbackOptions: BatchRecommendFeedbackRequestParams = {
                APIKey: generateUUID(),
                documentsFeedbacks: [{
                    sourceId: generate(),
                    targetId: generate(),
                }],
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.batch(recommendFeedbackOptions);


        } catch (error) {
            expect(error.message).toEqual(MESSAGES.PLATFORM_NAME_IS_REQUIRED)
        }
    });




    test('missing api key', async () => {
        try {

            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);


        } catch (error) {
            expect([
                MESSAGES.API_KEY_IS_REQUIRED,
                MESSAGES.JWT_TOKEN_IS_REQUIRED,
            ]).toContain(error.message);
        }
    });


    test('missing api key in batch', async () => {
        try {

            const recommendFeedbackOptions: BatchRecommendFeedbackRequestParams = {
                platformName: generateUUID(),
                documentsFeedbacks: [{
                    sourceId: generate(),
                    targetId: generate(),
                }],
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.batch(recommendFeedbackOptions);


        } catch (error) {
            expect([
                MESSAGES.API_KEY_IS_REQUIRED,
                MESSAGES.JWT_TOKEN_IS_REQUIRED,
            ]).toContain(error.message);
        }
    });

});

describe('Passing required fields and compare', () => {

    test('passing only required fields', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            platformName: generate(),
            APIKey: generateUUID(),
            documentFeedback: {
                sourceId: generate(),
                targetId: generate(),
            }
        };

        const lablebRequest = await LablebRequestBuilder();
        const { url, method, headers, params, body } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${recommendFeedbackOptions.platformName}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/recommend/${process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER}/feedback/hits`;

        expect(expectedURL).toEqual(url);
        expect(method).toEqual('POST');

        expect(params.apikey).toEqual(recommendFeedbackOptions.APIKey);

        expect(body).toEqual([recommendFeedbackOptions.documentFeedback]);
    });
});


describe('Passing optional fields and compare', () => {


    test('passing all optional fields', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            platformName: generate(),
            APIKey: generateUUID(),
            indexName: generate(),
            recommendHandler: generate(),
            jwtToken: generateUUID(),
            documentFeedback: {

                sourceId: generate(),
                sourceTitle: generate(),
                sourceUrl: 'https://example.com',

                targetId: generate(),
                targetTitle: generate(),
                targetUrl: 'https://example.com',

                itemOrder: 5,
                sessionId: generate(),
                userCountry: 'AF',
                userId: '42',
                userIp: '192.168.1.1'
            }
        };

        const lablebRequest = await LablebRequestBuilder();
        const { url, method, headers, params, body } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${recommendFeedbackOptions.platformName}/indices/${recommendFeedbackOptions.indexName}/recommend/${recommendFeedbackOptions.recommendHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
        expect(method).toEqual('POST');

        expect(headers['Authorization']).toEqual(`Bearer ${recommendFeedbackOptions.jwtToken}`);

        expect(params.apikey).toEqual(recommendFeedbackOptions.APIKey);

        expect(body).toEqual([recommendFeedbackOptions.documentFeedback]);
    });
});




describe('passing public/private options', () => {

    const globalOptions: GlobalRequestOptions = {
        indexName: generate(),
        APIKey: generateUUID(),
        recommendHandler: generate(),
        jwtToken: generateUUID(),
        platformName: generate(),
    }

    let lablebRequest: LablebRequestBuilderType;

    beforeAll(async () => {
        lablebRequest = await LablebRequestBuilder(globalOptions);
    });

    test('using public index name', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            APIKey: generateUUID(),
            documentFeedback: {
                sourceId: generate(),
                targetId: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${globalOptions.indexName}/recommend/${globalOptions.recommendHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using private index name', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            indexName: generate(),
            APIKey: generateUUID(),
            documentFeedback: {

                sourceId: generate(),
                targetId: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${recommendFeedbackOptions.indexName}/recommend/${globalOptions.recommendHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using public platform name', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            APIKey: generateUUID(),
            documentFeedback: {

                sourceId: generate(),
                targetId: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${globalOptions.indexName}/recommend/${globalOptions.recommendHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using private platform name', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            platformName: generate(),
            indexName: generate(),
            APIKey: generateUUID(),
            documentFeedback: {

                sourceId: generate(),
                targetId: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${recommendFeedbackOptions.platformName}/indices/${recommendFeedbackOptions.indexName}/recommend/${globalOptions.recommendHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using public recommend handler', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            APIKey: generateUUID(),
            documentFeedback: {

                sourceId: generate(),
                targetId: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${globalOptions.indexName}/recommend/${globalOptions.recommendHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using private recommend handler', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            recommendHandler: generate(),
            APIKey: generateUUID(),
            documentFeedback: {

                sourceId: generate(),
                targetId: generate(),
            }
        };

        const { url } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${globalOptions.indexName}/recommend/${recommendFeedbackOptions.recommendHandler}/feedback/hits`;

        expect(expectedURL).toEqual(url);
    });


    test('using public jwt', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            documentFeedback: {
                sourceId: generate(),
                targetId: generate(),
            }
        };

        const { headers } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);


        expect(headers['Authorization']).toEqual(`Bearer ${globalOptions.jwtToken}`);
    });


    test('using private jwt', async () => {

        const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
            jwtToken: generateUUID(),
            documentFeedback: {
                sourceId: generate(),
                targetId: generate(),
            }
        };

        const { headers } = await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${recommendFeedbackOptions.jwtToken}`);
    });

});




describe('Validate bad inputs', () => {

    test('invalid source id', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: 242.2,
                    targetId: generate(),
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.SOURCE_ID_IS_INVALID_DOCUMENT_ID)
        }
    });


    test('invalid target id', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: 242.2,
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.TARGET_ID_IS_INVALID_DOCUMENT_ID)
        }
    });

    test('invalid document order', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                    itemOrder: 5.2,
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.ITEM_ORDER_IS_NOT_INTEGER)
        }
    });


    test('invalid document url 1', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                    sourceUrl: 'htts;//example.com',
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.URL_IS_NOT_VALID)
        }
    });

    test('invalid document url 2', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                    targetUrl: 'htts;//example.com',
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.URL_IS_NOT_VALID)
        }
    });



    test('invalid document source title', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                    sourceTitle: ''
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.SOURCE_TITLE_LENGTH_IS_INVALID)
        }
    });



    test('invalid document target title', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                    targetTitle: ''
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.TARGET_TITLE_LENGTH_IS_INVALID)
        }
    });


  

    test('invalid user id', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                    userId: true as any,
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.INVALID_USER_ID)
        }
    });


    test('invalid user ip', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                    userIp: '455.168.1.1'
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.INVALID_IP_ADDRESS)
        }
    });



    test('invalid user country 1', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                    userCountry: '',
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID)
        }
    });


    test('invalid user country 2', async () => {
        try {
            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: generate(),
                    targetId: generate(),
                    userCountry: 'VSX',
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID)
        }
    });


    test('source id equal target id', async () => {
        try {
            const oneString = generate();

            const recommendFeedbackOptions: SingleRecommendFeedbackRequestParams = {
                platformName: generate(),
                APIKey: generateUUID(),
                indexName: generate(),
                recommendHandler: generate(),
                jwtToken: generateUUID(),
                documentFeedback: {
                    sourceId: oneString,
                    targetId: oneString,
                }
            };

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.feedback.recommend.single(recommendFeedbackOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.TARGET_ID_MUST_BE_DIFFERENT_THAN_SOURCE_ID)
        }
    });
});