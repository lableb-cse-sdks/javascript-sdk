import { appendJwtTokenHeader, customIdentity, customPickBy } from '../../utils';
import { RecommendFeedbackDocument } from '../feedback/feedback.document.type';
import { GlobalRequestOptions } from '../main/main.request.type';
import { buildBatchRecommendFeedbackRequestSchema } from './recommend-feedback.schema';
import { BatchRecommendFeedbackRequestParams, BatchRecommendFeedbackRequestResult, SingleRecommendFeedbackRequestParams } from './recommend-feedback.type';


export async function buildSingleRecommendFeedbackRequest(this: GlobalRequestOptions, params: SingleRecommendFeedbackRequestParams): Promise<BatchRecommendFeedbackRequestResult> {

    const { documentFeedback, ...rest } = params;

    const options: any = {
        ...rest,
    }

    if (documentFeedback) {
        options.documentsFeedbacks = [documentFeedback];
    }

    return buildBatchRecommendFeedbackRequest.bind(this)(options);
}


export async function buildBatchRecommendFeedbackRequest(this: GlobalRequestOptions, params: BatchRecommendFeedbackRequestParams): Promise<BatchRecommendFeedbackRequestResult> {


    const validatedParams = await buildBatchRecommendFeedbackRequestSchema.validate({
        ...params,
        indexName: params.indexName || this?.indexName || process.env.GLOBAL_DEFAULT_INDEX_NAME,
        platformName: params.platformName || this?.platformName,
        recommendHandler: params.recommendHandler || this?.recommendHandler || process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER,
        APIKey: params.APIKey || this?.APIKey,
        jwtToken: params.jwtToken || this?.jwtToken,
        documentsFeedbacks: params.documentsFeedbacks,
    });


    const {
        indexName,
        platformName,
        recommendHandler,
        APIKey,
        jwtToken,
        documentsFeedbacks,
    } = validatedParams;


    return {
        method: 'POST',
        url: `${process.env.API_BASE_URL}/projects/${platformName}/indices/${indexName}/recommend/${recommendHandler}/feedback/hits`,
        params: customPickBy(
            {
                apikey: APIKey,
            },
            customIdentity
        ),
        headers: appendJwtTokenHeader(jwtToken, {}),
        body: documentsFeedbacks as RecommendFeedbackDocument[],
    }
}

