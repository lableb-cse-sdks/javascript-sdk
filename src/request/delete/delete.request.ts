import { appendJwtTokenHeader, customIdentity, customPickBy } from '../../utils';
import { GlobalRequestOptions } from '../main/main.request.type';
import { buildIndexingRequestSchema } from './delete.schema';
import { DeleteRequestParams, DeleteRequestResult } from './delete.type';


export async function buildDeleteRequest(this: GlobalRequestOptions, params: DeleteRequestParams): Promise<DeleteRequestResult> {


    const validatedParams = await buildIndexingRequestSchema.validate({
        indexName: params.indexName || this?.indexName || process.env.GLOBAL_DEFAULT_INDEX_NAME,
        platformName: params.platformName || this?.platformName,
        indexingAPIKey: params.indexingAPIKey || this?.indexingAPIKey,
        jwtToken: params.jwtToken || this?.jwtToken,
        documentId: params.documentId,
    });


    const {
        indexName,
        platformName,
        indexingAPIKey,
        jwtToken,
        documentId,
    } = validatedParams;



    return {
        method: 'DELETE',
        url: `${process.env.API_BASE_URL}/projects/${platformName}/indices/${indexName}/documents/${documentId}`,
        params: customPickBy(
            {
                apikey: indexingAPIKey,
            },
            customIdentity
        ),
        headers: appendJwtTokenHeader(jwtToken, {}),
    }
}