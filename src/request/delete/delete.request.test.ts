import { DeleteRequestParams } from './delete.type';
import { v4 as generateUUID } from 'uuid';
import { generate } from "randomstring";
import { LablebRequestBuilder } from '../main/main.request';
import { MESSAGES } from '../../config/messages';
import { GlobalRequestOptions, LablebRequestBuilderType } from '../main/main.request.type';



describe('Missing required fields', () => {

    test('missing platform name', async () => {

        try {

            const lablebRequest = await LablebRequestBuilder();

            const deleteOptions: DeleteRequestParams = {
                documentId: 1,
                indexName: generate(),
                indexingAPIKey: generateUUID(),
            }

            await lablebRequest.delete(deleteOptions);

            throw new Error('unexpected error');

        } catch (error) {

            expect(error.message).toEqual(MESSAGES.PLATFORM_NAME_IS_REQUIRED)
        }
    });



    test('missing document id', async () => {

        try {

            const lablebRequest = await LablebRequestBuilder();

            const deleteOptions: any = {
                platformName: generate(),
                indexName: generate(),
                indexingAPIKey: generateUUID(),
            }

            await lablebRequest.delete(deleteOptions);

            throw new Error('unexpected error');

        } catch (error) {

            expect(error.message).toEqual(MESSAGES.DOCUMENT_ID_IS_REQUIRED)
        }
    });



    test('missing index api key or jwt token', async () => {

        try {

            const lablebRequest = await LablebRequestBuilder();

            const deleteOptions: DeleteRequestParams = {
                documentId: 2,
                platformName: generate(),
                indexName: generate(),
            }

            await lablebRequest.delete(deleteOptions);

            throw new Error('unexpected error');

        } catch (error) {

            expect([
                MESSAGES.JWT_TOKEN_IS_REQUIRED,
                MESSAGES.INDEXING_API_KEY_IS_REQUIRED
            ]).toContain(error.message);
        }
    });


});


/*************************/


describe('Pass required fields and compare', () => {

    test('compare all required fields', async () => {

        const lablebRequest = await LablebRequestBuilder();

        const deleteOptions: DeleteRequestParams = {
            documentId: 44,
            platformName: generate(),
            indexName: generate(),
            indexingAPIKey: generateUUID(),
        }

        const { url, method, params } = await lablebRequest.delete(deleteOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${deleteOptions.platformName}/indices/${deleteOptions.indexName}/documents/${deleteOptions.documentId}`;

        expect(url).toEqual(expectedURL);

        expect(method).toEqual('DELETE');

        expect(params.apikey).toEqual(deleteOptions.indexingAPIKey);
    });

});


/*************************/


describe('Pass optional fields and compare', () => {

    test('compare all optional fields', async () => {

        const lablebRequest = await LablebRequestBuilder();

        const deleteOptions: DeleteRequestParams = {
            documentId: 2,
            platformName: generate(),
            indexName: generate(),
            indexingAPIKey: generateUUID(),
            jwtToken: generateUUID(),
        }

        const { headers } = await lablebRequest.delete(deleteOptions);
        expect(headers['Authorization']).toEqual(`Bearer ${deleteOptions.jwtToken}`);

    });

});




describe('Test global/private options', () => {

    const globalOptions: GlobalRequestOptions = {
        platformName: generate(),
        indexName: generate(),
        indexingAPIKey: generateUUID(),
        jwtToken: generateUUID(),
    }

    let lablebRequest: LablebRequestBuilderType;

    beforeAll(async () => {
        lablebRequest = await LablebRequestBuilder(globalOptions);;
    });


    test('using global platform name', async () => {

        const deleteOptions: DeleteRequestParams = {
            documentId: 4,
            indexName: generate(),
            indexingAPIKey: generateUUID(),
        }

        const { url } = await lablebRequest.delete(deleteOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${deleteOptions.indexName}/documents/${deleteOptions.documentId}`;

        expect(url).toEqual(expectedURL);
    });



    test('using private platform name', async () => {

        const deleteOptions: DeleteRequestParams = {
            documentId: 4,
            indexName: generate(),
            indexingAPIKey: generateUUID(),
            platformName: generate(),
        }

        const { url } = await lablebRequest.delete(deleteOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${deleteOptions.platformName}/indices/${deleteOptions.indexName}/documents/${deleteOptions.documentId}`;

        expect(url).toEqual(expectedURL);
    });


    test('using global index name', async () => {

        const deleteOptions: DeleteRequestParams = {
            documentId: 4,
            indexingAPIKey: generateUUID(),
            platformName: generate(),
        }

        const { url } = await lablebRequest.delete(deleteOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${deleteOptions.platformName}/indices/${globalOptions.indexName}/documents/${deleteOptions.documentId}`;

        expect(url).toEqual(expectedURL);
    });


    test('using private index name', async () => {

        const deleteOptions: DeleteRequestParams = {
            documentId: 4,
            indexingAPIKey: generateUUID(),
            platformName: generate(),
            indexName: generate(),
        }

        const { url } = await lablebRequest.delete(deleteOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${deleteOptions.platformName}/indices/${deleteOptions.indexName}/documents/${deleteOptions.documentId}`;

        expect(url).toEqual(expectedURL);
    });


    test('using global indexing api key', async () => {

        const deleteOptions: DeleteRequestParams = {
            documentId: 4,
            indexName: generate(),
            platformName: generate(),
        }

        const { params } = await lablebRequest.delete(deleteOptions);

        expect(params.apikey).toEqual(globalOptions.indexingAPIKey);
    });

    test('using private indexing api key', async () => {

        const deleteOptions: DeleteRequestParams = {
            documentId: 4,
            indexName: generate(),
            platformName: generate(),
            indexingAPIKey: generateUUID(),
        }

        const { params } = await lablebRequest.delete(deleteOptions);

        expect(params.apikey).toEqual(deleteOptions.indexingAPIKey);
    });


    test('using global jwt token', async () => {

        const deleteOptions: DeleteRequestParams = {
            documentId: 4,
            indexName: generate(),
            indexingAPIKey: generateUUID(),
            platformName: generate(),
        }

        const { headers } = await lablebRequest.delete(deleteOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${globalOptions.jwtToken}`)
    });


    test('using private jwt token', async () => {

        const deleteOptions: DeleteRequestParams = {
            documentId: 4,
            indexName: generate(),
            jwtToken: generateUUID(),
            platformName: generate(),
        }

        const { headers } = await lablebRequest.delete(deleteOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${deleteOptions.jwtToken}`)
    });
});


describe('Validate Bad inputs', () => {

    test('bad id', async () => {
        try {
            const deleteOptions: DeleteRequestParams = {
                documentId: 4.52,
                indexName: generate(),
                jwtToken: generateUUID(),
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.delete(deleteOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.IS_INVALID_DOCUMENT_ID)
        }
    });


});
