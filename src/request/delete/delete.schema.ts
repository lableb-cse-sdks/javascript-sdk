import * as yup from 'yup';
import { isTruthyString, isValidDocumentIdForYup } from '../../utils';
import { MESSAGES } from '../../config/messages';
import { DeleteRequestParams } from './delete.type';


export const buildIndexingRequestSchema: yup.SchemaOf<DeleteRequestParams> =
    yup.object()
        .shape({

            indexingAPIKey: yup
                .string()
                .test('test-if-can-authenticate',
                    MESSAGES.INDEXING_API_KEY_IS_REQUIRED,
                    function testIfCanAuthenticate(value, context) {

                        if (!value && !context.parent.jwtToken)
                            return false;

                        return true;
                    }),

            jwtToken: yup.string().optional(),

            platformName: yup.string().required(MESSAGES.PLATFORM_NAME_IS_REQUIRED),
            indexName: yup.string().required(MESSAGES.INDEX_NAME_IS_REQUIRED),

            documentId: yup.string().required(MESSAGES.DOCUMENT_ID_IS_REQUIRED).test('test-id', MESSAGES.IS_INVALID_DOCUMENT_ID, isValidDocumentIdForYup),

        },
            // to allow circular dependency
            [['jwtToken', 'indexingAPIKey']]
        );
