import { BuildRequestResult } from "../../types";


export interface DeleteRequestParams {

    jwtToken?: string,
    indexingAPIKey?: string,

 
    platformName?: string,
    indexName?: string,

    documentId: number | string,
}


export interface DeleteRequestResult extends BuildRequestResult {

    params: {
        apikey?: string,
    },

}