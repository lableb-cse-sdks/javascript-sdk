import { appendJwtTokenHeader, convertFacetsObjectIntoString, customIdentity, customPickBy } from '../../utils';
import { GlobalRequestOptions } from '../main/main.request.type';
import { buildSearchRequestSchema } from './search.schema';
import { SearchRequestParams, SearchRequestResult, InputFacets } from './search.type';


export async function buildSearchRequest(this: GlobalRequestOptions, params: SearchRequestParams): Promise<SearchRequestResult> {


    const validatedParams = await buildSearchRequestSchema.validate({
        ...params,
        indexName: params.indexName || this?.indexName || process.env.GLOBAL_DEFAULT_INDEX_NAME,
        platformName: params.platformName || this?.platformName,
        searchHandler: params.searchHandler || this?.searchHandler || process.env.GLOBAL_DEFAULT_SEARCH_HANDLER,
        APIKey: params.APIKey || this?.APIKey,
        jwtToken: params.jwtToken || this?.jwtToken,
    });


    const {
        indexName,
        platformName,
        searchHandler,
        APIKey,
        jwtToken,
        query,
        facets,
        ...restOfOptions
    } = validatedParams;


    let facetsString = '';
    if (params.facets){
        const facetsStringResult = convertFacetsObjectIntoString(params.facets);
        facetsString = facetsStringResult ? `?${facetsStringResult}` : '';
    }

    return {
        method: 'GET',
        url: `${process.env.API_BASE_URL}/projects/${platformName}/indices/${indexName}/search/${searchHandler}${facetsString}`,
        params: customPickBy(
            {
                apikey: APIKey,
                q: query,
                ...restOfOptions
            },
            customIdentity
        ),
        headers: appendJwtTokenHeader(jwtToken, {})
    }
}