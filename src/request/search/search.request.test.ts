import { v4 as generateUUID } from 'uuid';
import { generate } from "randomstring";
import { SearchRequestParams } from './search.type';
import { LablebRequestBuilder } from '../main/main.request';
import { MESSAGES } from '../../config/messages';
import { createAndTestSearchURL } from '../../utils/test.utils/test.utils';
import { GlobalRequestOptions, LablebRequestBuilderType } from '../main/main.request.type';


describe('Missing required fields', () => {

    test('missing api key or jwt token', async () => {

        try {

            const options: SearchRequestParams = {
                query: generate(),
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.search(options);

            throw new Error('unexpected error');

        } catch (error) {
            expect([
                MESSAGES.JWT_TOKEN_IS_REQUIRED,
                MESSAGES.API_KEY_IS_REQUIRED,
            ]).toContain(error.message);
        }
    });



    test('missing query', async () => {

        try {

            const options: SearchRequestParams = {
                APIKey: generateUUID(),
                query: '',
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.search(options);

            throw new Error('unexpected error');

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.QUERY_IS_REQUIRED)
        }
    });


    test('missing platform name', async () => {

        try {

            const options: SearchRequestParams = {
                APIKey: generateUUID(),
                query: generate(),
                platformName: '',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.search(options);

            throw new Error('unexpected error');

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.PLATFORM_NAME_IS_REQUIRED)
        }
    });

});


describe('Pass required fields and compare', () => {



    test('passing all required fields', async () => {

        const searchOptions: SearchRequestParams = {
            APIKey: generateUUID(),
            query: generate(),
            platformName: generate(),
        }

        const lablebRequest = await LablebRequestBuilder();
        const { params } = await lablebRequest.search(searchOptions);


        const expectedURL = `${process.env.API_BASE_URL}/projects/${searchOptions.platformName}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/search/${process.env.GLOBAL_DEFAULT_SEARCH_HANDLER}`;

        await createAndTestSearchURL({
            expectedURL,
            globalOptions: {},
            searchOptions
        });

        expect(params.q).toEqual(searchOptions.query);
        expect(params.apikey).toEqual(searchOptions.APIKey);

    });

});




describe('Pass optional fields and compare', () => {


    test('passing all optional fields', async () => {

        const searchOptions: SearchRequestParams = {
            APIKey: generateUUID(),
            query: generate(),
            platformName: generate(),

            searchHandler: generate(),
            indexName: generate(),
            jwtToken: generateUUID(),


            userCountry: generate(2).toUpperCase(),
            filter: 'email:some@gmail.com',
            limit: 53,
            sessionId: generate(7),
            skip: 4,
            userId: '24',
            userIp: '172.110.64.4',
            sort: 'asc test',
        }


        const lablebRequest = await LablebRequestBuilder();
        const { params, headers } = await lablebRequest.search(searchOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${searchOptions.platformName}/indices/${searchOptions.indexName}/search/${searchOptions.searchHandler}`;

        await createAndTestSearchURL({
            expectedURL,
            globalOptions: {},
            searchOptions
        });


        expect(params.q).toEqual(searchOptions.query);
        expect(params.apikey).toEqual(searchOptions.APIKey);
        expect(headers['Authorization']).toEqual(`Bearer ${searchOptions.jwtToken}`);

        expect(params.userCountry).toEqual(searchOptions.userCountry);
        expect(params.filter).toEqual(searchOptions.filter);
        expect(params.limit).toEqual(searchOptions.limit);
        expect(params.sessionId).toEqual(searchOptions.sessionId);
        expect(params.skip).toEqual(searchOptions.skip);
        expect(params.userId).toEqual(searchOptions.userId);
        expect(params.userIp).toEqual(searchOptions.userIp);
        expect(params.sort).toEqual(searchOptions.sort);
    });


    test('passing all optional fields with facets', async () => {

        const searchOptions: SearchRequestParams = {
            APIKey: generateUUID(),
            query: generate(),
            platformName: generate(),

            searchHandler: generate(),
            indexName: generate(),
            jwtToken: generateUUID(),


            userCountry: generate(2).toUpperCase(),
            filter: 'email:some@gmail.com',
            limit: 53,
            sessionId: generate(7),
            skip: 4,
            userId: '24',
            userIp: '172.110.64.4',
            sort: 'asc test',

            facets: {
                tags: ['greeting', 'lableb']
            }
        }


        const lablebRequest = await LablebRequestBuilder();
        const { params, headers } = await lablebRequest.search(searchOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${searchOptions.platformName}/indices/${searchOptions.indexName}/search/${searchOptions.searchHandler}?tags=greeting&tags=lableb`;

        await createAndTestSearchURL({
            expectedURL,
            globalOptions: {},
            searchOptions
        });


        expect(params.q).toEqual(searchOptions.query);
        expect(params.apikey).toEqual(searchOptions.APIKey);
        expect(headers['Authorization']).toEqual(`Bearer ${searchOptions.jwtToken}`);

        expect(params.userCountry).toEqual(searchOptions.userCountry);
        expect(params.filter).toEqual(searchOptions.filter);
        expect(params.limit).toEqual(searchOptions.limit);
        expect(params.sessionId).toEqual(searchOptions.sessionId);
        expect(params.skip).toEqual(searchOptions.skip);
        expect(params.userId).toEqual(searchOptions.userId);
        expect(params.userIp).toEqual(searchOptions.userIp);
        expect(params.sort).toEqual(searchOptions.sort);
    });

});


describe('Test global/private options', () => {

    const globalOptions: GlobalRequestOptions = {
        platformName: generate(),
        indexName: generate(),
        APIKey: generateUUID(),
        jwtToken: generateUUID(),
        searchHandler: generate(),
    }

    let lablebRequest: LablebRequestBuilderType;

    beforeAll(async () => {
        lablebRequest = await LablebRequestBuilder(globalOptions);;
    });


    test('using global platform name', async () => {

        const searchOptions: SearchRequestParams = {
            query: generate(),
            searchHandler: generate(),
            indexName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${searchOptions.indexName}/search/${searchOptions.searchHandler}`;

        await createAndTestSearchURL({
            expectedURL,
            globalOptions,
            searchOptions
        });
    });


    test('using private platform name', async () => {

        const searchOptions: SearchRequestParams = {
            query: generate(),
            searchHandler: generate(),
            indexName: generate(),
            platformName: generate()
        }


        const expectedURL = `${process.env.API_BASE_URL}/projects/${searchOptions.platformName}/indices/${searchOptions.indexName}/search/${searchOptions.searchHandler}`;

        await createAndTestSearchURL({
            expectedURL,
            globalOptions,
            searchOptions
        });
    });

    test('using global search handler', async () => {

        const searchOptions: SearchRequestParams = {
            query: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${searchOptions.platformName}/indices/${searchOptions.indexName}/search/${globalOptions.searchHandler}`;

        await createAndTestSearchURL({
            expectedURL,
            globalOptions,
            searchOptions
        });
    });


    test('using private search handler', async () => {

        const searchOptions: SearchRequestParams = {
            query: generate(),
            searchHandler: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${searchOptions.platformName}/indices/${searchOptions.indexName}/search/${searchOptions.searchHandler}`;

        await createAndTestSearchURL({
            expectedURL,
            globalOptions,
            searchOptions
        });
    });


    test('using global api key', async () => {

        const searchOptions: SearchRequestParams = {
            query: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const { params } = await lablebRequest.search(searchOptions);

        expect(params.apikey).toEqual(globalOptions.APIKey);
    });


    test('using private api key', async () => {

        const searchOptions: SearchRequestParams = {
            query: generate(),
            searchHandler: generate(),
            indexName: generate(),
            platformName: generate(),
            APIKey: generateUUID(),
        }

        const { params } = await lablebRequest.search(searchOptions);
        expect(params.apikey).toEqual(searchOptions.APIKey);
    });



    test('using global jwt token', async () => {

        const searchOptions: SearchRequestParams = {
            query: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const { headers } = await lablebRequest.search(searchOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${globalOptions.jwtToken}`);
    });


    test('using private jwt token', async () => {

        const searchOptions: SearchRequestParams = {
            query: generate(),
            searchHandler: generate(),
            indexName: generate(),
            platformName: generate(),
            jwtToken: generateUUID(),
        }

        const { headers } = await lablebRequest.search(searchOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${searchOptions.jwtToken}`);
    });
});



describe('Validate bad inputs', () => {


    test('validate query', async () => {
        try {
            const searchOptions: SearchRequestParams = {
                query: '',
                platformName: generate(),
                APIKey: generateUUID()
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.search(searchOptions);
        } catch (error) {
            expect(error.message).toEqual(MESSAGES.QUERY_IS_REQUIRED)
        }
    });




    test('validate user country 1', async () => {
        try {
            const searchOptions: SearchRequestParams = {
                query: generate(),
                searchHandler: generate(),
                indexName: generate(),
                platformName: generate(),
                jwtToken: generateUUID(),
                userCountry: '',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.search(searchOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID)
        }
    });


    test('validate user country 2', async () => {
        try {
            const searchOptions: SearchRequestParams = {
                query: generate(),
                searchHandler: generate(),
                indexName: generate(),
                platformName: generate(),
                jwtToken: generateUUID(),
                userCountry: 'FAV',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.search(searchOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID)
        }
    });


    test('validate user ip', async () => {
        try {
            const searchOptions: SearchRequestParams = {
                query: generate(),
                searchHandler: generate(),
                indexName: generate(),
                platformName: generate(),
                jwtToken: generateUUID(),
                userIp: '366.125.21.21',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.search(searchOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.INVALID_IP_ADDRESS)
        }
    });



    test('validate skip', async () => {
        try {
            const searchOptions: SearchRequestParams = {
                query: generate(),
                searchHandler: generate(),
                indexName: generate(),
                platformName: generate(),
                jwtToken: generateUUID(),
                skip: 4.5,
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.search(searchOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.SKIP_IS_NOT_INTEGER)
        }
    });


    test('validate limit', async () => {
        try {
            const searchOptions: SearchRequestParams = {
                query: generate(),
                searchHandler: generate(),
                indexName: generate(),
                platformName: generate(),
                jwtToken: generateUUID(),
                limit: 4.5,

            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.search(searchOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.LIMIT_IS_NOT_INTEGER)
        }
    });
});