import * as yup from 'yup';
import { MESSAGES } from '../../config/messages';
import { isTruthyString, isValidIntegerForYup } from '../../utils';
import { baseRequestSchema } from '../main/main.request.schema';
import { SearchRequestParams } from './search.type';


export const buildSearchRequestSchema: yup.SchemaOf<SearchRequestParams> =
    baseRequestSchema.concat(
        yup.object()
            .shape({

                APIKey: yup
                    .string()
                    .test('test-if-can-authenticate',
                        MESSAGES.API_KEY_IS_REQUIRED,
                        function testIfCanAuthenticate(value, context) {

                            if (!value && !context.parent.jwtToken)
                                return false;

                            return true;
                        }),

                jwtToken: yup.string().optional(),

                query: yup.string().required(MESSAGES.QUERY_IS_REQUIRED),

                searchHandler: yup.string().optional(),


                filter: yup.string().optional(),
                sort: yup.string().optional(),

                skip: yup.number().optional().test('test-integer', MESSAGES.SKIP_IS_NOT_INTEGER, isValidIntegerForYup),
                limit: yup.number().optional().test('test-integer', MESSAGES.LIMIT_IS_NOT_INTEGER, isValidIntegerForYup),

                facets: yup.object().shape({
                    key: yup.string().optional(),
                }).optional(),
            },
                // to allow circular dependency
                [['jwtToken', 'APIKey']]
            )
    );
