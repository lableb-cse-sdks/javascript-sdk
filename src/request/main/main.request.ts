import { buildBatchAutocompleteFeedbackRequest, buildSingleAutocompleteFeedbackRequest } from "../autocomplete-feedback/autocomplete-feedback.request";
import { buildAutocompleteRequest } from "../autocomplete/autocomplete.request";
import { buildDeleteRequest } from "../delete/delete.request";
import { buildIndexingRequest } from "../indexing/indexing.request";
import { buildBatchRecommendFeedbackRequest, buildSingleRecommendFeedbackRequest } from "../recommend-feedback/recommend-feedback.request";
import { buildRecommendRequest } from "../recommend/recommend.request";
import { buildBatchSearchFeedbackRequest, buildSingleSearchFeedbackRequest } from "../search-feedback/search-feedback.request";
import { buildSearchRequest } from "../search/search.request";
import { mainRequestSchema } from "./main.request.schema";
import { GlobalRequestOptions } from "./main.request.type";


export async function LablebRequestBuilder(options: GlobalRequestOptions = {}) {

    await mainRequestSchema.validate(options)

    return {
        index: buildIndexingRequest.bind(options),
        search: buildSearchRequest.bind(options),
        autocomplete: buildAutocompleteRequest.bind(options),
        recommend: buildRecommendRequest.bind(options),
        delete: buildDeleteRequest.bind(options),
        feedback: {
            search: {
                single: buildSingleSearchFeedbackRequest.bind(options),
                batch: buildBatchSearchFeedbackRequest.bind(options),
            },
            autocomplete: {
                single: buildSingleAutocompleteFeedbackRequest.bind(options),
                batch: buildBatchAutocompleteFeedbackRequest.bind(options),
            },
            recommend: {
                single: buildSingleRecommendFeedbackRequest.bind(options),
                batch: buildBatchRecommendFeedbackRequest.bind(options),
            },
        },
    }
}