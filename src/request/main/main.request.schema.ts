import * as yup from 'yup';
import { MESSAGES } from '../../config/messages';
import { BaseRequestParams, OptionalBaseRequestParams } from '../../types';
import { isValidDocumentIdForYup, isValidIntegerForYup, isValidIPAddress } from '../../utils';
import { GlobalRequestOptions } from './main.request.type';


export const mainRequestSchema: yup.SchemaOf<GlobalRequestOptions> = yup.object()
    .shape({

        jwtToken: yup.string().optional(),
        APIKey: yup.string().optional(),
        indexingAPIKey: yup.string().optional(),

        platformName: yup.string().optional(),

        indexName: yup.string().optional(),
        searchHandler: yup.string().optional(),
        autocompleteHandler: yup.string().optional(),
        recommendHandler: yup.string().optional(),
    });




export const optionalBaseRequestSchema: yup.SchemaOf<OptionalBaseRequestParams> = yup.object()
    .shape({
        sessionId: yup.string().optional(),
        userId: yup.string().optional().test('test-id', MESSAGES.INVALID_USER_ID, isValidDocumentIdForYup),
        userIp: yup.string().optional().test('test-ip', MESSAGES.INVALID_IP_ADDRESS, isValidIPAddress),
        userCountry: yup.string().length(2, MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID).optional().uppercase(),
    });



export const baseRequestSchema: yup.SchemaOf<BaseRequestParams> =
    optionalBaseRequestSchema.concat(
        yup.object()
            .shape({

                platformName: yup.string().required(MESSAGES.PLATFORM_NAME_IS_REQUIRED),
                indexName: yup.string().required(MESSAGES.INDEX_NAME_IS_REQUIRED),

                indexingAPIKey: yup.string().optional(),
                APIKey: yup.string().optional(),
                jwtToken: yup.string().optional(),

            })
    );