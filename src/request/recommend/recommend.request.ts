import { appendJwtTokenHeader, customIdentity, customPickBy } from '../../utils';
import { GlobalRequestOptions } from '../main/main.request.type';
import { buildRecommendRequestSchema } from './recommend.schema';
import { RecommendRequestParams, RecommendRequestResult } from './recommend.type';


export async function buildRecommendRequest(this: GlobalRequestOptions, params: RecommendRequestParams): Promise<RecommendRequestResult> {


    const validatedParams = await buildRecommendRequestSchema.validate({
        ...params,
        indexName: params.indexName || this?.indexName || process.env.GLOBAL_DEFAULT_INDEX_NAME,
        platformName: params.platformName || this?.platformName,
        recommendHandler: params.recommendHandler || this?.recommendHandler || process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER,
        APIKey: params.APIKey || this?.APIKey,
        jwtToken: params.jwtToken || this?.jwtToken,
    });


    const {
        indexName,
        platformName,
        recommendHandler,
        APIKey,
        jwtToken,
        ...restOfOptions
    } = validatedParams;


    return {
        method: 'GET',
        url: `${process.env.API_BASE_URL}/projects/${platformName}/indices/${indexName}/recommend/${recommendHandler}`,
        params: customPickBy(
            {
                apikey: APIKey,
                ...restOfOptions
            },
            customIdentity
        ),
        headers: appendJwtTokenHeader(jwtToken, {})
    }
}