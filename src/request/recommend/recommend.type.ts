import { BaseRequestParams, BuildRequestResult } from "../../types";


export interface RecommendRequestParams extends BaseRequestParams {

    id: number | string,

    recommendHandler?: string,

    APIKey?: string,

    limit?: number,
    filter?: string,
    sort?: string,
    title?: string,
    url?: string,
}



export interface RecommendRequestResult extends BuildRequestResult {

    params:
    Omit<RecommendRequestParams, "platformName" | "APIKey" | "indexName" | "recommendHandler" | "jwtToken">
    &
    {
        apikey?: string,
    },
}