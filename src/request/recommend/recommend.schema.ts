import * as yup from 'yup';
import { MESSAGES } from '../../config/messages';
import { isTruthyString, isValidDocumentIdForYup, isValidIntegerForYup } from '../../utils';
import { baseRequestSchema } from '../main/main.request.schema';
import { RecommendRequestParams } from './recommend.type';


export const buildRecommendRequestSchema: yup.SchemaOf<RecommendRequestParams> =
    baseRequestSchema.concat(
        yup.object()
            .shape({

                APIKey: yup
                    .string()
                    .test('test-if-can-authenticate',
                        MESSAGES.API_KEY_IS_REQUIRED,
                        function testIfCanAuthenticate(value, context) {

                            if (!value && !context.parent.jwtToken)
                                return false;

                            return true;
                        }),

                jwtToken: yup.string().optional(),

                id: yup.string()
                    .test('test-id', MESSAGES.IS_INVALID_DOCUMENT_ID, isValidDocumentIdForYup)
                    .required(MESSAGES.DOCUMENT_ID_IS_REQUIRED),
                    
                title: yup.string().optional(),
                url: yup.string().url(MESSAGES.URL_IS_NOT_VALID).optional(),

                recommendHandler: yup.string().optional(),

                filter: yup.string().optional(),
                sort: yup.string().optional(),
                limit: yup.number().optional().test('test-integer', MESSAGES.LIMIT_IS_NOT_INTEGER, isValidIntegerForYup),

            },
                // to allow circular dependency
                [['jwtToken', 'APIKey']]
            )
    );
