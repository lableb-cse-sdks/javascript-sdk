import { v4 as generateUUID } from 'uuid';
import { generate } from "randomstring";
import { RecommendRequestParams } from './recommend.type';
import { LablebRequestBuilder } from '../main/main.request';
import { MESSAGES } from '../../config/messages';
import { GlobalRequestOptions, LablebRequestBuilderType } from '../main/main.request.type';
import { createAndTestRecommendURL } from '../../utils/test.utils/test.utils';



describe('Missing required fields', () => {

    test('missing api key or jwt token', async () => {

        try {

            const options: RecommendRequestParams = {
                id: 2,
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(options);

            throw new Error('unexpected error');

        } catch (error) {
            expect([
                MESSAGES.JWT_TOKEN_IS_REQUIRED,
                MESSAGES.API_KEY_IS_REQUIRED,
            ]).toContain(error.message);
        }
    });



    test('missing id', async () => {

        try {

            const options: any = {
                APIKey: generateUUID(),
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(options);

            throw new Error('unexpected error');

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.DOCUMENT_ID_IS_REQUIRED)
        }
    });


    test('missing platform name', async () => {

        try {

            const options: RecommendRequestParams = {
                APIKey: generateUUID(),
                id: 5,
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(options);

            throw new Error('unexpected error');

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.PLATFORM_NAME_IS_REQUIRED)
        }
    });

});


describe('Pass required fields and compare', () => {



    test('passing all required fields', async () => {

        const recommendOptions: RecommendRequestParams = {
            APIKey: generateUUID(),
            id: 45,
            platformName: generate(),
        }

        const lablebRequest = await LablebRequestBuilder();
        const { params } = await lablebRequest.recommend(recommendOptions);


        const expectedURL = `${process.env.API_BASE_URL}/projects/${recommendOptions.platformName}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/recommend/${process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER}`;

        await createAndTestRecommendURL({
            expectedURL,
            globalOptions: {},
            recommendOptions
        });

        expect(params.id).toEqual(String(recommendOptions.id));
        expect(params.apikey).toEqual(recommendOptions.APIKey);

    });

});




describe('Pass optional fields and compare', () => {


    test('passing all optional fields', async () => {

        const recommendOptions: RecommendRequestParams = {
            APIKey: generateUUID(),
            id: 41,
            platformName: generate(),

            recommendHandler: generate(),
            indexName: generate(),
            jwtToken: generateUUID(),


            userCountry: 'AF',
            filter: 'email:some@gmail.com',
            limit: 3,
            sessionId: '3909aaf',
            userId: '24',
            userIp: '172.110.64.4',
            sort: 'asc test',

            title: generate(),
            url: 'https://www.google.com'
        }


        const lablebRequest = await LablebRequestBuilder();
        const { params, headers } = await lablebRequest.recommend(recommendOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${recommendOptions.platformName}/indices/${recommendOptions.indexName}/recommend/${recommendOptions.recommendHandler}`;

        await createAndTestRecommendURL({
            expectedURL,
            globalOptions: {},
            recommendOptions
        });


        expect(params.id).toEqual(String(recommendOptions.id));
        expect(params.apikey).toEqual(recommendOptions.APIKey);
        expect(headers['Authorization']).toEqual(`Bearer ${recommendOptions.jwtToken}`);

        expect(params.url).toEqual(recommendOptions.url);
        expect(params.title).toEqual(recommendOptions.title);

        expect(params.userCountry).toEqual(recommendOptions.userCountry);
        expect(params.filter).toEqual(recommendOptions.filter);
        expect(params.limit).toEqual(recommendOptions.limit);
        expect(params.sessionId).toEqual(recommendOptions.sessionId);
        expect(params.userId).toEqual(recommendOptions.userId);
        expect(params.userIp).toEqual(recommendOptions.userIp);
        expect(params.sort).toEqual(recommendOptions.sort);
    });

});


describe('Test global/private options', () => {

    const globalOptions: GlobalRequestOptions = {
        platformName: generate(),
        indexName: generate(),
        APIKey: generateUUID(),
        jwtToken: generateUUID(),
        recommendHandler: generate(),
    }

    let lablebRequest: LablebRequestBuilderType;

    beforeAll(async () => {
        lablebRequest = await LablebRequestBuilder(globalOptions);;
    });


    test('using global platform name', async () => {

        const recommendOptions: RecommendRequestParams = {
            id: 572,
            recommendHandler: generate(),
            indexName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${recommendOptions.indexName}/recommend/${recommendOptions.recommendHandler}`;

        await createAndTestRecommendURL({
            expectedURL,
            globalOptions,
            recommendOptions
        });

    });


    test('using private platform name', async () => {

        const recommendOptions: RecommendRequestParams = {
            id: 572,
            recommendHandler: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${recommendOptions.platformName}/indices/${recommendOptions.indexName}/recommend/${recommendOptions.recommendHandler}`;

        await createAndTestRecommendURL({
            expectedURL,
            globalOptions,
            recommendOptions
        });
    });

    test('using global recommend handler', async () => {

        const recommendOptions: RecommendRequestParams = {
            id: 572,
            indexName: generate(),
            platformName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${recommendOptions.platformName}/indices/${recommendOptions.indexName}/recommend/${globalOptions.recommendHandler}`;

        await createAndTestRecommendURL({
            expectedURL,
            globalOptions,
            recommendOptions
        });
    });


    test('using private recommend handler', async () => {

        const recommendOptions: RecommendRequestParams = {
            id: 572,
            recommendHandler: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${recommendOptions.platformName}/indices/${recommendOptions.indexName}/recommend/${recommendOptions.recommendHandler}`;

        await createAndTestRecommendURL({
            expectedURL,
            globalOptions,
            recommendOptions
        });
    });


    test('using global api key', async () => {

        const recommendOptions: RecommendRequestParams = {
            id: 572,
            indexName: generate(),
            platformName: generate(),
        }

        const { params } = await lablebRequest.recommend(recommendOptions);

        expect(params.apikey).toEqual(globalOptions.APIKey);
    });


    test('using private api key', async () => {

        const recommendOptions: RecommendRequestParams = {
            id: 572,
            recommendHandler: generate(),
            indexName: generate(),
            platformName: generate(),
            APIKey: generateUUID(),
        }

        const { params } = await lablebRequest.recommend(recommendOptions);
        expect(params.apikey).toEqual(recommendOptions.APIKey);
    });



    test('using global jwt token', async () => {

        const recommendOptions: RecommendRequestParams = {
            id: 572,
            indexName: generate(),
            platformName: generate(),
        }

        const { headers } = await lablebRequest.recommend(recommendOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${globalOptions.jwtToken}`);
    });


    test('using private jwt token', async () => {

        const recommendOptions: RecommendRequestParams = {
            id: 572,
            recommendHandler: generate(),
            indexName: generate(),
            platformName: generate(),
            jwtToken: generateUUID(),
        }

        const { headers } = await lablebRequest.recommend(recommendOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${recommendOptions.jwtToken}`);
    });
});




describe('Validate bad inputs', () => {

    test('validate user country 1', async () => {
        try {
            const recommendOptions: RecommendRequestParams = {
                APIKey: generateUUID(),
                id: 41,
                platformName: generate(),

                userCountry: '',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(recommendOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID);
        }
    });

    test('validate user country 2', async () => {
        try {
            const recommendOptions: RecommendRequestParams = {
                APIKey: generateUUID(),
                id: 41,
                platformName: generate(),

                userCountry: 'AFX',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(recommendOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID);
        }
    });


    test('validate user ip', async () => {
        try {
            const recommendOptions: RecommendRequestParams = {
                APIKey: generateUUID(),
                id: 41,
                platformName: generate(),

                userIp: '125.952.205.23',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(recommendOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.INVALID_IP_ADDRESS);
        }
    });


    test('validate url', async () => {
        try {
            const recommendOptions: RecommendRequestParams = {
                APIKey: generateUUID(),
                id: 41,
                platformName: generate(),

                url: '125.952.205.23',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(recommendOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.URL_IS_NOT_VALID);
        }
    });


    test('validate id', async () => {
        try {
            const recommendOptions: RecommendRequestParams = {
                APIKey: generateUUID(),
                id: 41.3,
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(recommendOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.IS_INVALID_DOCUMENT_ID);
        }
    });




    test('validate limit', async () => {
        try {
            const recommendOptions: RecommendRequestParams = {
                APIKey: generateUUID(),
                id: 41,
                platformName: generate(),
                limit: 52.2,
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(recommendOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.LIMIT_IS_NOT_INTEGER);
        }
    });


    test('validate user id', async () => {
        try {
            const recommendOptions: RecommendRequestParams = {
                APIKey: generateUUID(),
                id: 41,
                platformName: generate(),
                userId: true as any,
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.recommend(recommendOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.INVALID_USER_ID);
        }
    });
});
