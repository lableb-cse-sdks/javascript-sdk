import { appendJwtTokenHeader, customIdentity, customPickBy } from '../../utils';
import { FeedbackDocument } from '../feedback/feedback.document.type';
import { GlobalRequestOptions } from '../main/main.request.type';
import { buildBatchSearchFeedbackRequestSchema } from './search-feedback.schema';
import { BatchSearchFeedbackRequestParams, BatchSearchFeedbackRequestResult, SingleSearchFeedbackRequestParams } from './search-feedback.type';


export async function buildSingleSearchFeedbackRequest(this: GlobalRequestOptions, params: SingleSearchFeedbackRequestParams): Promise<BatchSearchFeedbackRequestResult> {

    const { documentFeedback, ...rest } = params;

    const options: any = {
        ...rest,
    }

    if (documentFeedback) {
        options.documentsFeedbacks = [documentFeedback];
    }

    return buildBatchSearchFeedbackRequest.bind(this)(options);
}


export async function buildBatchSearchFeedbackRequest(this: GlobalRequestOptions, params: BatchSearchFeedbackRequestParams): Promise<BatchSearchFeedbackRequestResult> {

    const validatedParams = await buildBatchSearchFeedbackRequestSchema.validate({
        ...params,
        indexName: params.indexName || this?.indexName || process.env.GLOBAL_DEFAULT_INDEX_NAME,
        platformName: params.platformName || this?.platformName,
        searchHandler: params.searchHandler || this?.searchHandler || process.env.GLOBAL_DEFAULT_SEARCH_HANDLER,
        APIKey: params.APIKey || this?.APIKey,
        jwtToken: params.jwtToken || this?.jwtToken,
        documentsFeedbacks: params.documentsFeedbacks,
    });


    const {
        indexName,
        platformName,
        searchHandler,
        APIKey,
        jwtToken,
        documentsFeedbacks,
    } = validatedParams;


    return {
        method: 'POST',
        url: `${process.env.API_BASE_URL}/projects/${platformName}/indices/${indexName}/search/${searchHandler}/feedback/hits`,
        params: customPickBy(
            {
                apikey: APIKey,
            },
            customIdentity
        ),
        headers: appendJwtTokenHeader(jwtToken, {}),
        body: documentsFeedbacks as FeedbackDocument[],
    }
}

