import * as yup from 'yup';
import { MESSAGES } from '../../config/messages';
import { isTruthyString, isValidIntegerForYup } from '../../utils';
import { baseRequestSchema } from '../main/main.request.schema';
import { AutocompleteRequestParams } from './autocomplete.type';



export const buildAutocompleteRequestSchema: yup.SchemaOf<AutocompleteRequestParams> =
    baseRequestSchema.concat(
        yup.object()
            .shape({

                APIKey: yup
                    .string()
                    .test('test-if-can-authenticate',
                        MESSAGES.API_KEY_IS_REQUIRED,
                        function testIfCanAuthenticate(value, context) {

                            if (!value && !context.parent.jwtToken)
                                return false;

                            return true;
                        }),


                jwtToken: yup.string().optional(),

                query: yup.string().required(MESSAGES.QUERY_IS_REQUIRED),

                autocompleteHandler: yup.string().optional(),

                filter: yup.string().optional(),

                sort: yup.string().optional(),
                limit: yup.number().optional().test('test-integer', MESSAGES.LIMIT_IS_NOT_INTEGER, isValidIntegerForYup),

            },
                // to allow circular dependency
                [['jwtToken', 'APIKey']]
            )
    );
