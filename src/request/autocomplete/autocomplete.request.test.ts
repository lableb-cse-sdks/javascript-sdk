import { MESSAGES } from "../../config/messages";
import { AutocompleteRequestParams } from "./autocomplete.type";
import { LablebRequestBuilder } from "../main/main.request";
import { GlobalRequestOptions, LablebRequestBuilderType } from "../main/main.request.type";
import { createAndTestAutocompleteURL } from "../../utils/test.utils/test.utils";
import { v4 as generateUUID } from 'uuid';
import { generate } from "randomstring";




describe('Missing required fields', () => {

    test('missing api key or jwt token', async () => {

        try {

            const options: AutocompleteRequestParams = {
                query: generate(),
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.autocomplete(options);

            throw new Error('unexpected error');

        } catch (error) {
            expect([
                MESSAGES.JWT_TOKEN_IS_REQUIRED,
                MESSAGES.API_KEY_IS_REQUIRED,
            ]).toContain(error.message);
        }
    });



    test('missing query', async () => {

        try {

            const options: AutocompleteRequestParams = {
                APIKey: generateUUID(),
                query: '',
                platformName: generate(),
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.autocomplete(options);

            throw new Error('unexpected error');

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.QUERY_IS_REQUIRED)
        }
    });


    test('missing platform name', async () => {

        try {

            const options: AutocompleteRequestParams = {
                APIKey: generateUUID(),
                query: generate(),
                platformName: '',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.autocomplete(options);

            throw new Error('unexpected error');

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.PLATFORM_NAME_IS_REQUIRED)
        }
    });

});


describe('Pass required fields and compare', () => {



    test('passing all required fields', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            APIKey: generateUUID(),
            query: generate(),
            platformName: generate(),
        }

        const lablebRequest = await LablebRequestBuilder();
        const { params } = await lablebRequest.autocomplete(autocompleteOptions);


        const expectedURL = `${process.env.API_BASE_URL}/projects/${autocompleteOptions.platformName}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/autocomplete/${process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER}`;

        await createAndTestAutocompleteURL({
            expectedURL,
            globalOptions: {},
            autocompleteOptions
        });

        expect(params.q).toEqual(autocompleteOptions.query);
        expect(params.apikey).toEqual(autocompleteOptions.APIKey);

    });

});




describe('Pass optional fields and compare', () => {


    test('passing all optional fields', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            APIKey: generateUUID(),
            query: generate(),
            platformName: generate(),

            autocompleteHandler: generate(),
            indexName: generate(),
            jwtToken: generateUUID(),


            userCountry: 'AF',
            filter: 'email:some@gmail.com',
            limit: 3,
            sessionId: generate(7),
            userId: '24',
            userIp: '172.110.64.4',
            sort: 'asc test',

        }


        const lablebRequest = await LablebRequestBuilder();
        const { params, headers } = await lablebRequest.autocomplete(autocompleteOptions);

        const expectedURL = `${process.env.API_BASE_URL}/projects/${autocompleteOptions.platformName}/indices/${autocompleteOptions.indexName}/autocomplete/${autocompleteOptions.autocompleteHandler}`;

        await createAndTestAutocompleteURL({
            expectedURL,
            globalOptions: {},
            autocompleteOptions
        });


        expect(params.q).toEqual(autocompleteOptions.query);
        expect(params.apikey).toEqual(autocompleteOptions.APIKey);
        expect(headers['Authorization']).toEqual(`Bearer ${autocompleteOptions.jwtToken}`);

        expect(params.userCountry).toEqual(autocompleteOptions.userCountry);
        expect(params.filter).toEqual(autocompleteOptions.filter);
        expect(params.limit).toEqual(autocompleteOptions.limit);
        expect(params.sessionId).toEqual(autocompleteOptions.sessionId);
        expect(params.userId).toEqual(autocompleteOptions.userId);
        expect(params.userIp).toEqual(autocompleteOptions.userIp);
        expect(params.sort).toEqual(autocompleteOptions.sort);
    });

});


describe('Test global/private options', () => {

    const globalOptions: GlobalRequestOptions = {
        platformName: generate(),
        indexName: generate(),
        APIKey: generateUUID(),
        jwtToken: generateUUID(),
        autocompleteHandler: generate(),
    }

    let lablebRequest: LablebRequestBuilderType;

    beforeAll(async () => {
        lablebRequest = await LablebRequestBuilder(globalOptions);;
    });


    test('using global platform name', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            query: generate(),
            autocompleteHandler: generate(),
            indexName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${globalOptions.platformName}/indices/${autocompleteOptions.indexName}/autocomplete/${autocompleteOptions.autocompleteHandler}`;

        await createAndTestAutocompleteURL({
            expectedURL,
            globalOptions,
            autocompleteOptions
        });
    });


    test('using private platform name', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            query: generate(),
            autocompleteHandler: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${autocompleteOptions.platformName}/indices/${autocompleteOptions.indexName}/autocomplete/${autocompleteOptions.autocompleteHandler}`;

        await createAndTestAutocompleteURL({
            expectedURL,
            globalOptions,
            autocompleteOptions
        });
    });

    test('using global search handler', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            query: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${autocompleteOptions.platformName}/indices/${autocompleteOptions.indexName}/autocomplete/${globalOptions.autocompleteHandler}`;

        await createAndTestAutocompleteURL({
            expectedURL,
            globalOptions,
            autocompleteOptions
        });
    });


    test('using private search handler', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            query: generate(),
            autocompleteHandler: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const expectedURL = `${process.env.API_BASE_URL}/projects/${autocompleteOptions.platformName}/indices/${autocompleteOptions.indexName}/autocomplete/${autocompleteOptions.autocompleteHandler}`;

        await createAndTestAutocompleteURL({
            expectedURL,
            globalOptions,
            autocompleteOptions
        });
    });


    test('using global api key', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            query: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const { params } = await lablebRequest.autocomplete(autocompleteOptions);

        expect(params.apikey).toEqual(globalOptions.APIKey);
    });


    test('using private api key', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            query: generate(),
            autocompleteHandler: generate(),
            indexName: generate(),
            platformName: generate(),
            APIKey: generateUUID(),
        }

        const { params } = await lablebRequest.autocomplete(autocompleteOptions);

        expect(params.apikey).toEqual(autocompleteOptions.APIKey);
    });



    test('using global jwt token', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            query: generate(),
            indexName: generate(),
            platformName: generate(),
        }

        const { headers } = await lablebRequest.autocomplete(autocompleteOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${globalOptions.jwtToken}`);
    });


    test('using private jwt token', async () => {

        const autocompleteOptions: AutocompleteRequestParams = {
            query: generate(),
            autocompleteHandler: generate(),
            indexName: generate(),
            platformName: generate(),
            jwtToken: generateUUID(),
        }

        const { headers } = await lablebRequest.autocomplete(autocompleteOptions);

        expect(headers['Authorization']).toEqual(`Bearer ${autocompleteOptions.jwtToken}`);
    });
});







describe('Validate bad inputs', () => {

    test('validate user country 1', async () => {
        try {
            const autocompleteOptions: AutocompleteRequestParams = {
                APIKey: generateUUID(),
                query: generate(),
                platformName: generate(),

                autocompleteHandler: generate(),
                indexName: generate(),
                jwtToken: generateUUID(),

                userCountry: '',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.autocomplete(autocompleteOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID);
        }
    });

    test('validate user country 2', async () => {
        try {
            const autocompleteOptions: AutocompleteRequestParams = {
                APIKey: generateUUID(),
                query: generate(),
                platformName: generate(),

                autocompleteHandler: generate(),
                indexName: generate(),
                jwtToken: generateUUID(),

                userCountry: 'AFBV',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.autocomplete(autocompleteOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.COUNTRY_NAME_LENGTH_IS_INVALID);
        }
    });


    test('validate user ip', async () => {
        try {
            const autocompleteOptions: AutocompleteRequestParams = {
                APIKey: generateUUID(),
                query: generate(),
                platformName: generate(),

                autocompleteHandler: generate(),
                indexName: generate(),
                jwtToken: generateUUID(),

                userIp: '6565.112.12.32',
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.autocomplete(autocompleteOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.INVALID_IP_ADDRESS);
        }
    });


    test('validate limit', async () => {
        try {
            const autocompleteOptions: AutocompleteRequestParams = {
                APIKey: generateUUID(),
                query: generate(),
                platformName: generate(),

                autocompleteHandler: generate(),
                indexName: generate(),
                jwtToken: generateUUID(),

                limit: 42.2,
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.autocomplete(autocompleteOptions);

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.LIMIT_IS_NOT_INTEGER);
        }
    });


    test('validate user id', async () => {
        try {
            const autocompleteOptions: AutocompleteRequestParams = {
                APIKey: generateUUID(),
                query: generate(),
                platformName: generate(),

                autocompleteHandler: generate(),
                indexName: generate(),
                jwtToken: generateUUID(),

                userId: true as any,
            }

            const lablebRequest = await LablebRequestBuilder();
            await lablebRequest.autocomplete(autocompleteOptions);

        } catch (error) {
            expect(error.message).toContain('must be a `string`');
        }
    });
});