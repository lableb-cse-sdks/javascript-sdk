import { appendJwtTokenHeader, customIdentity, customPickBy } from "../../utils";
import { GlobalRequestOptions } from "../main/main.request.type";
import { buildAutocompleteRequestSchema } from "./autocomplete.schema";
import { AutocompleteRequestParams, AutocompleteRequestResult } from "./autocomplete.type";


export async function buildAutocompleteRequest(this: GlobalRequestOptions, params: AutocompleteRequestParams): Promise<AutocompleteRequestResult> {

    const validatedParams = await buildAutocompleteRequestSchema.validate({
        ...params,
        indexName: params.indexName || this?.indexName || process.env.GLOBAL_DEFAULT_INDEX_NAME,
        platformName: params.platformName || this?.platformName,
        autocompleteHandler: params.autocompleteHandler || this?.autocompleteHandler || process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER,
        APIKey: params.APIKey || this?.APIKey,
        jwtToken: params.jwtToken || this?.jwtToken,
    });

    const {
        indexName,
        platformName,
        autocompleteHandler,
        APIKey,
        jwtToken,
        query,
        ...restOfOptions
    } = validatedParams;


    return {
        method: 'GET',
        url: `${process.env.API_BASE_URL}/projects/${platformName}/indices/${indexName}/autocomplete/${autocompleteHandler}`,
        params: customPickBy(
            {
                apikey: APIKey,
                q: query,
                ...restOfOptions
            },
            customIdentity
        ),
        headers: appendJwtTokenHeader(jwtToken, {})
    }
}