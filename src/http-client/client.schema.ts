import * as yup from 'yup';
import { MESSAGES } from '../config/messages';
import { isValidBodyType, isValidHttpMethod } from '../utils';


export const lablebHttpClientSchema = yup.object().shape({

    method: yup
        .string()
        .required()
        .test('test-http-method', MESSAGES.WRONG_HTTP_METHOD, isValidHttpMethod),

    url: yup.string().required(),

    body: yup.mixed().when('method', {
        is: (value: string) => value == 'GET' || value == 'DELETE',
        then: yup.mixed()
            .test('test-not-required',
                MESSAGES.BODY_IS_NOT_ALLOWED_FOR_GET_OR_DELETE,
                (value: any) => {
                    if (value) return false;
                    return true;
                }),
        otherwise: yup.mixed().required(MESSAGES.BODY_IS_REQUIRED),
    }),

    bodyType: yup
        .string()
        .optional()
        .default('json')
        .test('test-body-type', MESSAGES.BODY_TYPE_SHOULD_BE_JSON_OR_FORM_DATA, isValidBodyType),

    params: yup.object().optional(),
    headers: yup.object().optional(),
});
