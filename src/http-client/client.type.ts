export type ALLOWED_HTTP_METHOD = 'GET' | 'POST' | 'PUT' | 'DELETE';

export interface LablebHttpClientParams {

    method: ALLOWED_HTTP_METHOD,
    url: string,

    bodyType?: 'json' | 'form-data',

    body?: any,
    params?: any,
    headers?: any,
}