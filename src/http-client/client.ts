import axios from "axios";
import { customIdentity, customPickBy, toFormData } from "../utils";
import { lablebHttpClientSchema } from "./client.schema";
import { LablebHttpClientParams } from "./client.type";



export async function LablebHttpClient(options: LablebHttpClientParams) {

    const {
        method,
        url,
        body,
        bodyType,
        params,
        headers,
    } = await lablebHttpClientSchema.validate(options);



    // const _body = bodyType == 'json' ? body : toFormData(body);
    const _body = body;
    const _headers = {
        ...headers,
        'Content-Type': bodyType == 'form-data' ? 'multipart/form-data' : 'application/json',
    }
    
    return axios(
        customPickBy({
            method,
            url,
            headers: _headers,
            data: _body,
            params,
        }, customIdentity))
        .then((response: any) => response.data);
}
