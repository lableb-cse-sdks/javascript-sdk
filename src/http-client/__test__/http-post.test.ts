import { MESSAGES } from '../../config/messages';
import { LablebHttpClient } from '..';


describe('Test Node.js http client: POST', () => {


    test("make post request", async () => {

        const response = await LablebHttpClient({
            url: `http://localhost/test-post`,
            method: 'POST',
            body: {},
        });

        expect(response.text).toEqual("hello world");
    });


    test("make post request without body should throw error", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-post`,
                method: 'POST',
            });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.BODY_IS_REQUIRED)
        }
    });


    test("make post request with wrong body as string should throw error", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-post`,
                method: 'POST',
                body: '',
            });

        } catch (error) {
            expect(error.message).toBeTruthy();
        }
    });


    test("make post request with wrong body type should throw error", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-post`,
                method: 'POST',
                body: {},
                bodyType: 'any' as any,

            });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.BODY_TYPE_SHOULD_BE_JSON_OR_FORM_DATA)
        }
    });



    test("make post request with form data body type", async () => {

        const response = await LablebHttpClient({
            url: `http://localhost/test-post`,
            method: 'POST',
            body: { test: 'test' },
            bodyType: 'form-data'
        });

        expect(response.text).toEqual("hello world");
    });


});
