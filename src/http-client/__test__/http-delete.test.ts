import { MESSAGES } from '../../config/messages';
import { LablebHttpClient } from '..';


describe('Test Node.js http client: DELETE', () => {

    

    test("make delete request", async () => {

        const response = await LablebHttpClient({
            url: `http://localhost/test-delete`,
            method: 'DELETE',
        });

        expect(response.text).toEqual("hello world");
    });


    test("make delete request with body should fail", async () => {

        try {

            await LablebHttpClient({
                url: `http://localhost/test-delete`,
                method: 'DELETE',
                body: {},
            });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.BODY_IS_NOT_ALLOWED_FOR_GET_OR_DELETE)
        }
    });


    test("make delete request with wrong headers type should fail", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-delete`,
                method: 'DELETE',
                headers: ''
            });

        } catch (error) {
            expect(error.message).toBeTruthy();
        }
    });

    test("make delete request with wrong params type should fail", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-delete`,
                method: 'DELETE',
                params: '',
            });

        } catch (error) {
            expect(error.message).toBeTruthy();
        }
    });


});
