import { MESSAGES } from '../../config/messages';
import { LablebHttpClient } from '..';


describe('Test Node.js http client: GET', () => {


    test("make get request", async () => {

        const response = await LablebHttpClient({
            url: `http://localhost/test-get`,
            method: 'GET',
        });

        expect(response.text).toEqual("hello world");
    });


    test("make get request with body should fail", async () => {

        try {

            await LablebHttpClient({
                url: `http://localhost/test-get`,
                method: 'GET',
                body: {},
            });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.BODY_IS_NOT_ALLOWED_FOR_GET_OR_DELETE);
        }
    });


    test("make get request with wrong headers type should fail", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-get`,
                method: 'GET',
                headers: ''
            });

        } catch (error) {
            expect(error.message).toBeTruthy();
        }
    });

    test("make get request with wrong params type should fail", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-get`,
                method: 'GET',
                params: '',
            });

        } catch (error) {
            expect(error.message).toBeTruthy();
        }
    });


});
