import { MESSAGES } from '../../config/messages';
import { LablebHttpClient } from '..';


describe('Test Node.js http client: PUT', () => {


    test("make put request", async () => {

        const response = await LablebHttpClient({
            url: `http://localhost/test-put`,
            method: 'PUT',
            body: {},
        });

        expect(response.text).toEqual("hello world");
    });


    test("make put request without body should throw error", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-put`,
                method: 'PUT',
            });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.BODY_IS_REQUIRED);
        }
    });


    test("make put request with wrong body as string should throw error", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-put`,
                method: 'PUT',
                body: '',
            });

        } catch (error) {
            expect(error.message).toBeTruthy();
        }
    });


    test("make put request with wrong body type should throw error", async () => {

        try {
            await LablebHttpClient({
                url: `http://localhost/test-put`,
                method: 'PUT',
                body: {},
                bodyType: 'any' as any,

            });

        } catch (error) {
            expect(error.message).toEqual(MESSAGES.BODY_TYPE_SHOULD_BE_JSON_OR_FORM_DATA)
        }
    });


    test("make put request with form data body type", async () => {

        const response = await LablebHttpClient({
            url: `http://localhost/test-put`,
            method: 'PUT',
            body: { test: 'test' },
            bodyType: 'form-data'
        });

        expect(response.text).toEqual("hello world");
    });
});
