import { AutocompleteFeedbackResponse } from '../core/autocomplete-feedback/autocomplete-feedback.type';
import { AutocompleteResponse } from '../core/autocomplete/autocomplete.type';
import { DeleteResponse } from '../core/delete/delete.type';
import { IndexingResponse } from '../core/indexing/indexing.type';
import { RecommendFeedbackResponse } from '../core/recommend-feedback/recommend-feedback.type';
import { RecommendResponseWithFeedback } from '../core/recommend/recommend.type';
import { SearchFeedbackResponse } from '../core/search-feedback/search-feedback.type';
import { SearchResponse } from '../core/search/search.type';
import { AutocompleteRequestResult } from '../request/autocomplete/autocomplete.type';
import { RecommendRequestResult } from '../request/recommend/recommend.type';
import { SearchRequestResult } from '../request/search/search.type';
const searchExampleResponse = require('./example-response/search.example.json');


export const MOCK_BACKEND_API = {

    SEARCH_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/search/${process.env.GLOBAL_DEFAULT_SEARCH_HANDLER}`,

    search: function _search({
        params,
    }: {
        params: SearchRequestResult['params'],
    }): SearchResponse {

        return searchExampleResponse.response;
    },



    AUTOCOMPLETE_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/autocomplete/${process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER}`,

    autocomplete: function _autocomplete({
        params,
    }: {
        params: AutocompleteRequestResult['params'],
    }): AutocompleteResponse {


        return searchExampleResponse.response;
    },


    RECOMMEND_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/recommend/${process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER}`,

    recommend: function _recommend({
        params,
    }: {
        params: RecommendRequestResult['params'],
    }): RecommendResponseWithFeedback {


        return searchExampleResponse.response;
    },


    INDEX_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/documents`,

    index: function _index({
        params,
    }: {
        params: any
    }): IndexingResponse {


        return null;
    },


    DELETE_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/documents/1`,

    delete: function _delete({
        params,
    }: {
        params: any
    }): DeleteResponse {


        return null;
    },


    SEARCH_FEEDBACK_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/search/${process.env.GLOBAL_DEFAULT_SEARCH_HANDLER}/feedback/hits`,

    searchFeedback: function _searchFeedback({
        params,
    }: {
        params: any
    }): SearchFeedbackResponse {


        return null;
    },


    AUTOCOMPLETE_FEEDBACK_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/autocomplete/${process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER}/feedback/hits`,

    autocompleteFeedback: function _autocompleteFeedback({
        params,
    }: {
        params: any
    }): AutocompleteFeedbackResponse {


        return null;
    },



    RECOMMEND_FEEDBACK_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/recommend/${process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER}/feedback/hits`,

    recommendFeedback: function _recommendFeedback({
        params,
    }: {
        params: any
    }): RecommendFeedbackResponse {


        return null;
    },
}
