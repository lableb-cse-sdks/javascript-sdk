import { LablebClient } from './core';
import { UnPromise } from './types';

console.log('Lableb JS-SDK VERSION 2.0.6');


export { GlobalRequestOptions as LablebClientOptions } from './request/main/main.request.type';
export { SearchRequestParams as LablebSearchOptions } from './request/search/search.type';
export { AutocompleteRequestParams as LablebAutocompleteOptions } from './request/autocomplete/autocomplete.type';
export { RecommendRequestParams as LablebRecommendOptions } from './request/recommend/recommend.type';


export { LablebClient }


export type LablebClientSearchResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['search']>>['response'];
export type LablebClientAutocompleteResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['autocomplete']>>['response'];
export type LablebClientRecommendResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['recommend']>>['response'];

export type LablebClientIndexingResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['index']>>['response'];
export type LablebClientDeleteResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['delete']>>['response'];

export type LablebClientSingleSearchFeedbackResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['feedback']['search']['single']>>['response'];
export type LablebClientBatchSearchFeedbackResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['feedback']['search']['batch']>>['response'];

export type LablebClientSingleAutocompleteFeedbackResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['feedback']['autocomplete']['single']>>['response'];
export type LablebClientBatchAutocompleteFeedbackResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['feedback']['autocomplete']['batch']>>['response'];


export type LablebClientSingleRecommendFeedbackResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['feedback']['recommend']['single']>>['response'];
export type LablebClientBatchRecommendFeedbackResponse = UnPromise<ReturnType<UnPromise<ReturnType<typeof LablebClient>>['feedback']['recommend']['batch']>>['response'];


export { SearchRequestParams, SearchRequestResult } from './request/search/search.type';
export { AutocompleteRequestParams, AutocompleteRequestResult } from './request/autocomplete/autocomplete.type';
export { RecommendRequestParams, RecommendRequestResult } from './request/recommend/recommend.type';
export { IndexingRequestParams, IndexingRequestResult } from './request/indexing/indexing.type';
export { DeleteRequestParams, DeleteRequestResult } from './request/delete/delete.type';
export { BatchSearchFeedbackRequestParams, SingleSearchFeedbackRequestParams, BatchSearchFeedbackRequestResult } from './request/search-feedback/search-feedback.type';
export { BatchAutocompleteFeedbackRequestParams, SingleAutocompleteFeedbackRequestParams, BatchAutocompleteFeedbackRequestResult } from './request/autocomplete-feedback/autocomplete-feedback.type';
export { BatchRecommendFeedbackRequestParams, SingleRecommendFeedbackRequestParams, BatchRecommendFeedbackRequestResult } from './request/recommend-feedback/recommend-feedback.type';

