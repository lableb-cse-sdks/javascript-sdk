import { AutocompleteRequestParams } from "../../request/autocomplete/autocomplete.type";
import { LablebRequestBuilder } from "../../request/main/main.request";
import { GlobalRequestOptions } from "../../request/main/main.request.type";
import { RecommendRequestParams } from "../../request/recommend/recommend.type";
import { SearchRequestParams } from "../../request/search/search.type";

export async function createAndTestSearchURL({
    expectedURL,
    globalOptions,
    searchOptions,
}: {
    searchOptions: SearchRequestParams,
    globalOptions: GlobalRequestOptions,
    expectedURL: string
}) {

    const lablebRequest = await LablebRequestBuilder(globalOptions);
    const { url } = await lablebRequest.search(searchOptions);

    expect(url).toEqual(expectedURL);
}



export async function createAndTestAutocompleteURL({
    expectedURL,
    globalOptions,
    autocompleteOptions,
}: {
    autocompleteOptions: AutocompleteRequestParams,
    globalOptions: GlobalRequestOptions,
    expectedURL: string
}) {

    const lablebRequest = await LablebRequestBuilder(globalOptions);
    const { url } = await lablebRequest.autocomplete(autocompleteOptions);

    expect(url).toEqual(expectedURL);
}


export async function createAndTestRecommendURL({
    expectedURL,
    globalOptions,
    recommendOptions,
}: {
    recommendOptions: RecommendRequestParams,
    globalOptions: GlobalRequestOptions,
    expectedURL: string
}) {

    const lablebRequest = await LablebRequestBuilder(globalOptions);
    const { url } = await lablebRequest.recommend(recommendOptions);

    expect(url).toEqual(expectedURL);
}
