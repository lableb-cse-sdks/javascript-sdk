import { customIdentity, customPickBy } from './functional';

test('test custom identity function', () => {

    expect(customIdentity(3)).toEqual(3);
    expect(customIdentity(0)).toEqual('0');
    expect(customIdentity('')).toEqual(true);
    expect(customIdentity(false)).toEqual(false);
});

test('test custom pick by', () => {

    let input = {
        stringValue: 'hello',
        emptyString: '',
        zeroValue: 0,
        falsy: false,
    }

    let output = {
        stringValue: 'hello',
        emptyString: '',
        zeroValue: 0,
    }

    expect(customPickBy(input, customIdentity)).toEqual(output)
    expect(customPickBy(undefined, customIdentity)).toEqual(undefined)
});