/**
 * take an object of key values, it keeps only truthy values depending on
 * the predicate function passed
 */
export function customPickBy(obj: any, func: (v: any) => boolean) {

    if (!obj) return undefined;

    Object.keys(obj).forEach(key => {
        if (!func(obj[key])) {
            delete obj[key]
        }
    });

    return obj;
}


export function customIdentity(value: any) {

    /**
     * allow 0 values to exist and convert them to string
     */
    if (typeof value == 'number' && value == 0)
        return '0';

    if (typeof value == 'string' && value == '')
        return true;

    return value;
}