

export function isValidIPAddress(value?: string) {

    function isValidIPAddressString(ipAddress: string) {

        return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipAddress);
    }


    if (value && value.length)
        return isValidIPAddressString(value);

    return true;
}


export function isValidDocumentIdForYup(value: any) {

    if (typeof value == 'undefined')
        return true;

    if (typeof value != 'string' && typeof value != 'number')
        return false;

    if (typeof value == 'string' && value.length == 0)
        return false;


    return true;
}


export function isValidHttpMethod(value?: string) {

    if (value && !['GET', 'POST', 'PUT', 'DELETE'].includes(value))
        return false;

    return true;
}

export function isValidBodyType(value?: string) {

    if (value && !['json', 'form-data'].includes(value))
        return false;

    return true;
}



export function isTruthyString(value: string) {
    if (typeof value != 'string') return false;

    return value && value.length > 0;
}

export function isValidIntegerForYup(value?: number) {

    if (typeof value == 'undefined')
        return true;

    if (typeof value == 'number')
        return Number.isInteger(value);

    return false;
}