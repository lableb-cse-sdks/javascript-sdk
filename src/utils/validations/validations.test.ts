import {
    isTruthyString,
    isValidDocumentIdForYup,
    isValidIPAddress,
    isValidIntegerForYup,
    isValidBodyType,
    isValidHttpMethod
} from '../validations/validations';


test('test is truthy string', () => {

    expect(isTruthyString('')).toBeFalsy();
    expect(isTruthyString('test')).toBeTruthy();
    expect(isTruthyString({} as any)).toBeFalsy();

});


test('validate document id', () => {

    expect(isValidDocumentIdForYup(2)).toBeTruthy();
    expect(isValidDocumentIdForYup('test-document')).toBeTruthy();
    expect(isValidDocumentIdForYup('24294')).toBeTruthy();
    expect(isValidDocumentIdForYup(4.242)).toBeTruthy();

    expect(isValidDocumentIdForYup({})).toBeFalsy();
    expect(isValidDocumentIdForYup(null)).toBeFalsy();
    expect(isValidDocumentIdForYup(true)).toBeFalsy();
    expect(isValidDocumentIdForYup('')).toBeFalsy();


    // @todo think of possible fix for yup to make it falsy
    expect(isValidDocumentIdForYup(undefined)).toBeTruthy();
});

test('is valid ip address', () => {

    expect(isValidIPAddress('192.168.1.2')).toBeTruthy();
    expect(isValidIPAddress('192.168.1.288')).toBeFalsy();
    expect(isValidIPAddress('284.523.521.214')).toBeFalsy();

    // @todo need a fix, it should be falsy
    expect(isValidIPAddress('0.0.0.0')).toBeTruthy();

});


test('is integer', () => {

    expect(isValidIntegerForYup(2)).toBeTruthy();
    expect(isValidIntegerForYup(2.3)).toBeFalsy();
    expect(isValidIntegerForYup('23' as any)).toBeFalsy();
    expect(isValidIntegerForYup('' as any)).toBeFalsy();
    expect(isValidIntegerForYup({} as any)).toBeFalsy();
});


test('is valid body type', () => {

    expect(isValidBodyType('json')).toBeTruthy();
    expect(isValidBodyType('form-data')).toBeTruthy();
    expect(isValidBodyType('something')).toBeFalsy();
});

test('is valid http method', () => {

    expect(isValidHttpMethod('GET')).toBeTruthy();
    expect(isValidHttpMethod('POST')).toBeTruthy();
    expect(isValidHttpMethod('PUT')).toBeTruthy();
    expect(isValidHttpMethod('DELETE')).toBeTruthy();

    expect(isValidHttpMethod('PATCH')).toBeFalsy();
});