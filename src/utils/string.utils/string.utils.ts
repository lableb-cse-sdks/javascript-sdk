import { InputFacets } from "../../request/search/search.type";
import { customIdentity, customPickBy } from "../functional/functional";

export function camelCaseToSnackCaseObject(object: any) {

    /**
     * ref
     * https://stackoverflow.com/questions/54246477/how-to-convert-camelcase-to-snake-case-in-javascript
     */
    const _camelToSnakeCase = (str: string) => str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);


    let newObject: any = {};

    Object.keys(object).forEach(key => {
        const oldKey = key;
        const newKey = _camelToSnakeCase(key);
        newObject[newKey] = object[oldKey]
    });

    return newObject;
}




/**
 * convert a facets object shape into a string or repeated facets string like
 * 'tags=greeting&tags=hello' 
 */
export function convertFacetsObjectIntoString(facets: InputFacets) {

    const facetsInputs = customPickBy(facets, (facetsArray) =>
        facetsArray &&
        facetsArray.length &&
        facetsArray.filter(customIdentity).length > 0
    );

    let facetsString = '';

    for (const key of Object.keys(facetsInputs))
        for (const singleValue of facets[key])
            facetsString += `${key}=${singleValue}&`;

    // slice remove the last `&`
    return facetsString.slice(0, facetsString.length - 1);
}
