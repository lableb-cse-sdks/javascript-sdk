import { camelCaseToSnackCaseObject, convertFacetsObjectIntoString } from './string.utils';


test('test camel case to snake case convert', () => {

    const input = {
        helloWorld: 'hello world'
    }

    const output = {
        hello_world: 'hello world'
    }

    expect(camelCaseToSnackCaseObject(input)).toEqual(output);
});


test('test converting facets to string 1', () => {

    let outFacetsString = convertFacetsObjectIntoString({
        tags: ['greeting', 'hello']
    });

    const expected = 'tags=greeting&tags=hello';

    expect(expected).toEqual(outFacetsString);

});

test('test converting facets to string 2', () => {

    let outFacetsString = convertFacetsObjectIntoString({
        tags: ['greeting', 'hello'],
        categories: ['tech', 'start'],
    });

    const expected = 'tags=greeting&tags=hello&categories=tech&categories=start';

    expect(expected).toEqual(outFacetsString);

});