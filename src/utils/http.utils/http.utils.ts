// const FormData = require('form-data');


export function appendJwtTokenHeader(jwtToken?: string, existingHeaders: any = {}) {

    if (!jwtToken) return existingHeaders;

    return {
        ...existingHeaders,
        'Authorization': `Bearer ${jwtToken}`,
    }
}




/**
 * convert js object into form data object
 * 
 * @param {object} obj 
 * @deprecated
 */
export function toFormData(obj: any) {

    return obj;
    let formData = new FormData();

    Object.keys(obj).forEach(key => {

        let value = obj[key];

        formData.append(key, typeof value === 'object' ? value : String(value))
    });

    return formData;
}
