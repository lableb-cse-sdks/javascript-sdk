import { appendJwtTokenHeader, toFormData } from './http.utils';
import { v4 } from 'uuid';

test('should append token', () => {

    const token = v4();

    const input = {
        'Content-Type': 'application/json'
    }

    const output = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }

    expect(appendJwtTokenHeader(token, input)).toEqual(output);
});


// test('convert object to form data', () => {

//     const input = {
//         apikey: v4(),
//         indexName: 'test'
//     }

//     let outputFormData = toFormData(input);


//     let outputAsString = JSON.stringify(outputFormData._streams);

//     /**
//      * so bad test, because unfortunately the `form-data` lib don't have a get method yet in it
//      */
//     expect(outputAsString).toContain(`name=\\"indexName\\"`);
//     expect(outputAsString).toContain(`name=\\"apikey\\"`);
// });