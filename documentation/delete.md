# Lableb's Client | Delete API

After creating a new instance of Lableb's Client,

```js
const lablebClient = await LablebClient({
    APIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});
```

You can use the delete function as 

```js
await lablebClient.delete(options);
```

----------------------

## Options

All options has types validation built in for you, in case you mis-typed something, And if you're using [typescript](https://github.com/microsoft/TypeScript) the code editor will tell you for any mis-typed argument instantly.

Options are of three categories:
- delete specific
- globally shared, which can be used to override any global option for this delete function call

----------------------

### Delete specific options

| field       | type   | description |
| ----------- | ------ | ----------- |                                                                 
| documentId* | string  | document(data) Id you want to remove from Lableb's Cloud |                



----------------------



### Globally shared options

Passing these options to the delete function will override any globally passed options to the main `LablebClient` of the same name.

| field          | type   | description |
| -------------- | ------ | ----------- |
| platformName   | string | your platform name in small-letters |
| indexName      | string | the used index name for the index function |
| indexingAPIKey | string | your indexing API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken       | string | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |

