# Lableb's Client | Recommend Feedback API


After creating a new instance of Lableb's Client,

```js
const lablebClient = await LablebClient({
    APIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});
```

You can access the recommend feedback functions at `lablebClient.feedback.recommend`, where you have two types of feedbacks functions: `single` and `batch`

Use `lablebClient.feedback.recommend.single` to send one feedback at a time to Lableb, and use `lablebClient.feedback.recommend.batch` to send many feedbacks at the same time to Lableb.


# Options

## Single Feedback

| field              | type   | description |
| ------------------ | ------ | ----------- |                                                                 
| documentFeedback*  | object | feedback object(see below for details) |                
| platformName       | string | your platform name in small-letters |
| indexName          | string | the used index name for the recommend function |
| recommendHandler      | string | the used recommend handler for the recommend function |
| APIKey             | string | your recommend API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken           | string | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |



## Content of the feedback object: `documentFeedback`

| field       | type   | description |
| ----------- | ------ | ----------- |
| sourceId*   | string | Source data Id that is used to fetch the recommendations |
| sourceTitle | string | Source data Title that is used to fetch the recommendations |
| sourceUrl   | string | Source data URL that is used to fetch the recommendations |
| targetId*   | string | Target data Id that's the end-user interact with |
| targetTitle | string | Target data Title that's the end-user interact with |
| targetUrl   | string | Target data URL that's the end-user interact with |
| itemOrder   | string | document's order clicked on or chosen by the user |
| sessionId   | string | uniques session id of the end user |
| userId      | string | end user id |
| userIp      | string | end user IP address |
| userCountry | string | end user country code(ISO-3166) |


Example

```js
await lablebClient.feedback.recommend.single({
    documentFeedback: {
       
        sourceId: 141,
        sourceTitle: 'Searching for water',
        sourceUrl: 'https://example.com/articles/searching-for-water',

        targetId: 984,
        targetTitle: 'How to preserve water consumption by 20%',
        targetUrl: 'https://example.com/articles/preserve-water-consumption',

        itemOrder: 2,
        sessionId: 'a9VAm22',

        userCountry: 'AE',
        userId: 'um29ava-mv92na-kieyqn-annv1',
        userIp: '172.165.32.1'
    }
});
```



## Batch Feedback

Its options basically the same as `single` feedback options except using `documentsFeedbacks` as an array instead of `documentFeedback` as an object.

| field               | type   | description |
| ------------------- | ------ | ----------- |                                                                 
| documentsFeedbacks* | array  | array of feedback object(see below for details) |                
| platformName        | string | your platform name in small-letters |
| indexName           | string | the used index name for the recommend function |
| recommendHandler       | string | the used recommend handler for the recommend function |
| APIKey              | string | your recommend API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken            | string | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |


Example

```js
await lablebClient.feedback.recommend.batch({
    documentsFeedbacks: [
      {
        sourceId: 141,
        sourceTitle: 'Searching for water',
        sourceUrl: 'https://example.com/articles/searching-for-water',

        targetId: 984,
        targetTitle: 'How to preserve water consumption by 20%',
        targetUrl: 'https://example.com/articles/preserve-water-consumption',

        itemOrder: 2,
        sessionId: 'a9VAm22',

        userCountry: 'AE',
        userId: 'um29ava-mv92na-kieyqn-annv1',
        userIp: '172.165.32.1'
      },
      {
        sourceId: 141,
        sourceTitle: 'Searching for water',
        sourceUrl: 'https://example.com/articles/searching-for-water',

        targetId: 753,
        targetTitle: 'How to purify your water at home',
        targetUrl: 'https://example.com/articles/purify-your-water-at-home',

        itemOrder: 7,
        sessionId: 'a9VAm22',

        userCountry: 'AE',
        userId: 'um29ava-mv92na-kieyqn-annv1',
        userIp: '172.165.32.1'
      },
      ...
    ]
});
```