# Lableb's Client | Autocomplete Feedback API


After creating a new instance of Lableb's Client,

```js
const lablebClient = await LablebClient({
    APIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});
```

You can access the autocomplete feedback functions at `lablebClient.feedback.autocomplete`, where you have two types of feedbacks functions: `single` and `batch`

Use `lablebClient.feedback.autocomplete.single` to send one feedback at a time to Lableb, and use `lablebClient.feedback.autocomplete.batch` to send many feedbacks at the same time to Lableb.


# Options

## Single Feedback

| field              | type   | description |
| ------------------ | ------ | ----------- |                                                                 
| documentFeedback*  | object | feedback object(see below for details) |                
| platformName       | string | your platform name in small-letters |
| indexName          | string | the used index name for the autocomplete function |
| autocompleteHandler| string | the used autocomplete handler for the autocomplete function |
| APIKey             | string | your autocomplete API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken           | string | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |



## Content of the feedback object: `documentFeedback`

| field     | type   | description |
| --------- | ------ | ----------- |
| query*    | string | query used in the autocomplete function |
| itemId    | string | document's id clicked on or chosen by the user |
| itemOrder | string | document's order clicked on or chosen by the user |
| url       | string | document's url clicked on or chosen by the user | 
| sessionId | string | uniques session id of the end user |


Example

```js
await lablebClient.feedback.autocomplete.single({
    documentFeedback: {
      query: 'water',
      itemId: '37',
      itemOrder: '2',
      sessionId: '3mn6baLA',
      url: 'https://example.com/items/37'
    }
});
```



## Batch Feedback

Its options basically the same as `single` feedback options except using `documentsFeedbacks` as an array instead of `documentFeedback` as an object.

| field               | type   | description |
| ------------------- | ------ | ----------- |                                                                 
| documentsFeedbacks* | array  | array of feedback object(see below for details) |                
| platformName        | string | your platform name in small-letters |
| indexName           | string | the used index name for the autocomplete function |
| autocompleteHandler       | string | the used autocomplete handler for the autocomplete function |
| APIKey              | string | your autocomplete API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken            | string | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |


Example

```js
await lablebClient.feedback.autocomplete.batch({
    documentsFeedbacks: [
      {
        query: 'water',
        itemId: '37',
        itemOrder: '2',
        sessionId: '3mn6baL',
        url: 'https://example.com/items/37'
      },
      {
        query: 'food',
        itemId: '421',
        itemOrder: '8',
        sessionId: 'vA2ml2a',
        url: 'https://example.com/items/421'
      },
      {
        query: 'cold',
        itemId: '687',
        itemOrder: '5',
        sessionId: 'gavz@1m',
        url: 'https://example.com/items/687'
      }
    ]
});
```