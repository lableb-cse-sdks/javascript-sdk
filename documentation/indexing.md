# Lableb's Client | Indexing API

After creating a new instance of Lableb's Client,

```js
const lablebClient = await LablebClient({
    indexingAPIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});
```

You can use the index function as 

```js
await lablebClient.index(options);
```

----------------------

## Options

All options has types validation built in for you, in case you mis-typed something, And if you're using [typescript](https://github.com/microsoft/TypeScript) the code editor will tell you for any mis-typed argument instantly.

Options are of three categories:
- index specific
- globally shared, which can be used to override any global option for this indexing function call

----------------------

### index specific options

| field       | type   | description |
| ----------- | ------ | ----------- |                                                                 
| documents*  | array  | array of data you want to upload/add to Lableb's cloud |                


  - Documents syntax

    Each document must at least contain an `id` and should match your platform's schema that is defined at [Lableb Dashboard](https://dashboard.lableb.com), Go to indices page, select your index name, then navigate to `schema` page and see what fields your document object can or can't have in it.

    Example

    ```js
    await lablebClient.index({
        documents: [
            { id: "1", title: "Nice to search with Lableb" },
            { id: "2", title: "Forget your manual SQL search for ever" },
            ...
        ]
    });
    ```


----------------------



### Globally shared options

Passing these options to the index function will override any globally passed options to the main `LablebClient` of the same name.

| field          | type   | description |
| -------------- | ------ | ----------- |
| platformName   | string | your platform name in small-letters |
| indexName      | string | the used index name for the index function |
| indexingAPIKey | string | your indexing API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken       | string | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |



----------------------


> Note: How to update already indexed data
> 
> To update any existing data you have at Lableb, just re index it again, and it will be updated automatically for you.