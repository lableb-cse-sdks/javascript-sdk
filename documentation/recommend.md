# Lableb's Client | Recommend API

After creating a new instance of Lableb's Client,

```js
const lablebClient = await LablebClient({
    APIKey: process.env.API_KEY,
    platformName: process.env.PLATFORM_NAME,
});
```

You can use the recommend function as 

```js
const { code, response, time } = await lablebClient.recommend(options);

const recommendResults = response.results;
```

----------------------

## Options

All options has types validation built in for you, in case you mis-typed something, And if you're using [typescript](https://github.com/microsoft/TypeScript) the code editor will tell you for any mis-typed argument instantly.

Options are of three categories:
- recommend specific
- globally shared, which can be used to override any global option for this recommend function call
- user related

----------------------

### recommend specific options


| field   | type   | description |
| ------- | ------ | ----------- |                                                     
| id*     | string | document(data) id that you want similar document of |                
| title   | string | document(data) title that you want similar document of |
| url     | string | document(data) url that you want similar document of |
| filter  | string | add filters to your recommend request with a string syntax |
| sort    | string | sort your results by specific field and specific order |
| limit   | number | limit your recommend results to some number |



  - Filters syntax

    Filter your data using string syntax that first has the filter name then the value you are filtering for.

    To quickly filter the documents for the tags `brilliant` and `great` you can send a facet object that   contains both    
    
    ```js
    await lablebClient.recommend({
        filters: "tags: brilliant, tags: great"
    });
    ```



  - Sort syntax

    You type the field name followed by the `asc` or `desc` to sort data by that field in an ascending or   descending order accordingly.
    
    Example:
    
    ```js
    await lablebClient.recommend({
        sort: "date desc"
    });
    ```


----------------------



### Globally shared options

Passing these options to the recommend function will override any globally passed options to the main `LablebClient` of the same name.

| field         | type   | description |
| ------------- | ------ | ----------- |
| platformName  | string | your platform name in small-letters |
| indexName     | string | the used index name for the recommend function |
| recommendHandler | string | the used recommend handler for the recommend function |
| APIKey        | string | your recommend API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken      | string | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |



----------------------


### User related options

Useful to track your users behavior later on, because knowing your users can help you better develop your platform/product.

| field         | type   | description |
| ------------- | ------ | ----------- |
| userId        | string | end user id |
| userIp        | string | end user IP address |
| userCountry   | string | end user country code(ISO-3166) |
| sessionId     | string | uniques session Id for your end user |


